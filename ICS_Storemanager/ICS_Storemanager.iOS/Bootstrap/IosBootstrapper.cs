﻿using Autofac;
using Xamarin.Forms;
using abmsoft.Mobile.Core.Interfaces;
using ICS_Storemanager.Bootstrap;

namespace ICS_Storemanager.iOS.Bootstrap
{
	public class IosBootstrapper : Bootstrapper
	{
		public IosBootstrapper(Xamarin.Forms.Application application) : base(application)
		{
		}


		protected override void ConfigureContainer(ContainerBuilder builder)
		{
			base.ConfigureContainer(builder);

          //  builder.RegisterType<DBPath>().As<IDBPath>();

        }
    }
}

