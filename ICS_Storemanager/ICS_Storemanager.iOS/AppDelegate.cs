﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using ICS_Storemanager.iOS.Bootstrap;

namespace ICS_Storemanager.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            #region Bootstrap App - Register all Services with AutoFAC

            // we keep a reference to theApp so we can use it in MessageCenter to send PushNotification-Messages
            // Should not be needed when using the Push-Plugin
            var theApp = new App();

            // We moved this to the platform, in order to be able to register platform-specific types too
            // We can do this by providing a platform-specific override of the Shared-Project's Bootstrapper and
            // add Type-Registrations there
            var bootstrapper = new IosBootstrapper(theApp);
            bootstrapper.Run();
            #endregion

            LoadApplication(theApp);

            return base.FinishedLaunching(app, options);
        }
    }
}
