using SQLite.Net;
using SQLite.Net.Platform.XamarinIOS;
using System.IO;
using abmsoft.mobile.core.Interfaces;
using SQLite.Net.Async;
using static ICS_Storemanager.IOS.Services.SQLiteVerbindung;

[assembly: Xamarin.Forms.Dependency(typeof(DatabaseConnection_IOS))]
namespace ICS_Storemanager.IOS.Services
{
    class SQLiteVerbindung
    {
        public class DatabaseConnection_IOS : IDatabaseConnection
        {
            string dbName = "Inventur_4.db3";
            string documentPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string path = string.Empty;

            public SQLiteConnection DbConnection()
            {
                path = Path.Combine(documentPath, dbName);

                SQLitePlatformIOS platform = new SQLitePlatformIOS();

                return new SQLiteConnection(platform, path);
            }
            public bool ExistsDatabase()
            {
                return File.Exists(path);
            }

            public void DropDatabase()
            {
                if (File.Exists(path))
                    File.Delete(path);
            }

            public string GetDeviceID()
            {
                var id = default(string);
                id = UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();
                return id;
            }
        }

    }
}