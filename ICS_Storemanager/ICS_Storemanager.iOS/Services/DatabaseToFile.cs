using System.IO;
using abmsoft.mobile.core.Interfaces;
using static ICS_Storemanager.Droid.Services.DatabaseToFile;
using abmsoft.Mobile.Core.Services;
using abmsoft.Mobile.Core.Models.Database;
using System.Collections.Generic;

[assembly: Xamarin.Forms.Dependency(typeof(DatabaseToFile_IOS))]
namespace ICS_Storemanager.Droid.Services
{
    class DatabaseToFile
    {
        public class DatabaseToFile_IOS : IDatabaseToFile
        {
            string documentPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            public void AllDataTableToFile()
            {
                WriteWareneingang();
                WriteWarenausgang();
                WriteInventur();
            }

            private void WriteWareneingang()
            {
                DataAccessService dbService = new DataAccessService();

                List<Wareneingang> items = dbService.QueryAllWareneingang();
                string csvdat = "wareneingang.csv";
                string filename = Path.Combine(documentPath, csvdat);

                try
                {
                    StreamWriter sr = new StreamWriter(filename, true, System.Text.Encoding.UTF8);

                    foreach (Wareneingang item in items)
                    {
                        string strdat = item.Positionsnummer + ";" + item.Benutzer + ";" + item.Bereich + ";" + item.Lager + ";" + item.Lieferscheinnummer;
                        sr.WriteLine(strdat);
                        dbService.DeleteRow(item);
                    }
                    sr.Close();
                    sr.Dispose();
                }
                catch (System.Exception ex)
                {
                    string meldung = ex.Message;
                }
            }
            private void WriteWarenausgang()
            {
                DataAccessService dbService = new DataAccessService();

                List<Warenausgang> items = dbService.QueryAllWarenausgang();
                string csvdat = "warenausgang.csv";
                string filename = Path.Combine(documentPath, csvdat);

                try
                {
                    StreamWriter sr = new StreamWriter(filename, true, System.Text.Encoding.UTF8);

                    foreach (Warenausgang item in items)
                    {
                        string strdat = item.Positionsnummer + ";" + item.Benutzer + ";" + item.Bereich + ";" + item.Lager;
                        sr.WriteLine(strdat);
                        dbService.DeleteRow(item);
                    }
                    sr.Close();
                    sr.Dispose();
                }
                catch (System.Exception ex)
                {
                    string meldung = ex.Message;
                }
            }

            private void WriteInventur()
            {
                DataAccessService dbService = new DataAccessService();

                List<Inventur> items = dbService.QueryAllInventur();
                string csvdat = "inventur.csv";
                string filename = Path.Combine(documentPath, csvdat);

                try
                {
                    StreamWriter sr = new StreamWriter(filename, true, System.Text.Encoding.UTF8);

                    foreach (Inventur item in items)
                    {
                        string strdat = item.Positionsnummer + ";" + item.Benutzer + ";" + item.Bereich + ";" + item.Lager;
                        sr.WriteLine(strdat);
                        dbService.DeleteRow(item);
                    }
                    sr.Close();
                    sr.Dispose();
                }
                catch (System.Exception ex)
                {
                    string meldung = ex.Message;
                }
            }

        }

    }
}