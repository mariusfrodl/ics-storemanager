﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace ICS_Storemanager.Droid
{
    [Activity(Label = "ICS_Storemanager", Theme ="@style/MyCustomTheme", Icon = "@drawable/icon", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        public App CurrentApp;
        private static MainActivity _mainActivity;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            _mainActivity = this;

            #region Bootstrap App - Register all Services with AutoFAC

            // we keep a reference to theApp so we can use it in MessageCenter to send PushNotification-Messages
            // Should not be needed when using the Push-Plugin
            CurrentApp = new App();

            // We moved this to the platform, in order to be able to register platform-specific types too
            // We can do this by providing a platform-specific override of the Shared-Project's Bootstrapper and
            // add Type-Registrations there
            var bootstrapper = new DroidBootstrapper(CurrentApp);
            bootstrapper.Run();
            #endregion
            try
            {
                 var pixels = Resources.DisplayMetrics.WidthPixels;
                var scale = Resources.DisplayMetrics.Density;

                var dps = (double)((pixels - 0.5f) / scale);

                var ScreenWidth = (int)dps;

                App.screenWidth = ScreenWidth;

                pixels = Resources.DisplayMetrics.HeightPixels;
                dps = (double)((pixels - 0.5f) / scale);

                var ScreenHeight = (int)dps;
                App.screenHeight = ScreenHeight;

                LoadApplication(CurrentApp);
            }
            catch (System.SystemException syex)
            {
                string message = syex.Message;
            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }
        }

        public static MainActivity getMainActivity()
        {
            return _mainActivity;
        }

    }
}

