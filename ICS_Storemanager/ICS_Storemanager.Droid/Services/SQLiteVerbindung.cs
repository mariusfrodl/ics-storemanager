using SQLite;
using System.IO;
using abmsoft.mobile.core.Interfaces;
using static ICS_Storemanager.Droid.Services.SQLiteVerbindung;

[assembly: Xamarin.Forms.Dependency(typeof(DatabaseConnection_Android))]
namespace ICS_Storemanager.Droid.Services
{
    class SQLiteVerbindung
    {
        public class DatabaseConnection_Android : IDatabaseConnection
        {
            string dbName = "ICS_Storemanager.sdf";
            //string documentPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string documentPath =  Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).AbsolutePath;
            public SQLiteConnection DbConnection()
            {
                string path = Path.Combine(documentPath, dbName);

                // platform = new SQLite..Platform.XamarinAndroid.SQLitePlatformAndroid();

                return new SQLiteConnection(path);
            }
            public bool ExistsDatabase()
            {
                string path = Path.Combine(documentPath, dbName);

                return File.Exists(path);
            }

            public void DropDatabase()
            {
                string path = Path.Combine(documentPath, dbName);

                if (File.Exists(path))
                    File.Delete(path);
            }

            public string GetDeviceID()
            {
                var id = default(string);
                id = Android.OS.Build.Serial;
                return id;
            }

        }

    }
}