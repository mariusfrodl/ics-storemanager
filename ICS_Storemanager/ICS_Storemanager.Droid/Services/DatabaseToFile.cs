using SQLite;
using System.IO;
using abmsoft.mobile.core.Interfaces;
using static ICS_Storemanager.Droid.Services.DatabaseToFile;
using abmsoft.Mobile.Core.Services;
using abmsoft.Mobile.Core.Models.Database;
using System.Collections.Generic;

[assembly: Xamarin.Forms.Dependency(typeof(DatabaseToFile_Android))]
namespace ICS_Storemanager.Droid.Services
{
    class DatabaseToFile
    {
        public class DatabaseToFile_Android : IDatabaseToFile
        {
            DataAccessService _dbService = new DataAccessService();
            string geraetename = string.Empty;
            string documentPath = string.Empty;
            string Datum = string.Empty;
            public void AllDataTableToFile()
            {
                geraetename = _dbService.GetGeraetename();
                documentPath = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).AbsolutePath;
                documentPath = documentPath + "/ICS_Storemanager";
                Datum = System.DateTime.Now.Day.ToString("00") + System.DateTime.Now.Month.ToString("00") + System.DateTime.Now.Year.ToString("0000") + System.DateTime.Now.Hour.ToString("00") + System.DateTime.Now.Minute.ToString("00") + System.DateTime.Now.Second.ToString("00");
                if (!Directory.Exists(documentPath))
                {
                    Directory.CreateDirectory(documentPath);
                }

                WriteWareneingang();
                WriteWarenausgang();
                WriteInventur();
                WriteBestellerfassung();
            }

            private void WriteWareneingang()
            {
                DataAccessService dbService = new DataAccessService();

                List<Wareneingang> items = dbService.QueryAllWareneingang();
                string csvdat = "wareneingang_" + geraetename + "_" + Datum + ".csv";
                string filename = Path.Combine(documentPath, csvdat);

                try
                {
                    if (items.Count > 0)
                    {
                        if (_dbService.GetDemoVersion())
                        {
                            if (_dbService.GetDemoAnzahl("1") == 5)
                            {
                                Config item = new Config() { ConfigID = "1", Bezeichnung = "DemoAnzWareneingang", Value = "1" };
                                _dbService.UpdateRow(item);
                            }
                        }

                        StreamWriter sr = new StreamWriter(filename, true, System.Text.Encoding.UTF8);

                        foreach (Wareneingang item in items)
                        {
                            string strdat = item.Positionsnummer + ";" + item.Artikelnummer + ";" + item.Menge.ToString() + ";" + item.Benutzer + ";" + item.Lager + ";" + item.Bereich + ";" + item.Lagerplatz + ";" + item.Lieferscheinnummer;
                            sr.WriteLine(strdat);
                            dbService.DeleteRow(item);
                        }
                        sr.Close();
                        sr.Dispose();
                    }
                }
                catch (System.Exception ex)
                {
                    string meldung = ex.Message;
                }
            }
            private void WriteWarenausgang()
            {
                DataAccessService dbService = new DataAccessService();

                List<Warenausgang> items = dbService.QueryAllWarenausgang();
                string csvdat = "warenausgang_" + geraetename + "_" + Datum + ".csv";
                string filename = Path.Combine(documentPath, csvdat);

                try
                {
                    if (items.Count > 0)
                    {
                        if (_dbService.GetDemoVersion())
                        {
                            if (_dbService.GetDemoAnzahl("2") == 5)
                            {
                                Config item = new Config() { ConfigID = "2", Bezeichnung = "DemoAnzWarenausgang", Value = "1" };
                                _dbService.UpdateRow(item);
                            }
                        }

                        StreamWriter sr = new StreamWriter(filename, true, System.Text.Encoding.UTF8);

                        foreach (Warenausgang item in items)
                        {

                            string strdat = item.Positionsnummer + ";" + item.Artikelnummer + ";" + item.Menge.ToString() + ";" + item.Benutzer + ";" + item.Lager + ";";
                            strdat += item.Bereich + ";" + item.Lagerplatz;
                            sr.WriteLine(strdat);
                            dbService.DeleteRow(item);
                        }
                        sr.Close();
                        sr.Dispose();
                    }
                }
                catch (System.Exception ex)
                {
                    string meldung = ex.Message;
                }
            }

            private void WriteInventur()
            {
                DataAccessService dbService = new DataAccessService();

                List<Inventur> items = dbService.QueryAllInventur();
                string csvdat = "inventur_" + geraetename + "_" + Datum + ".csv";
                string filename = Path.Combine(documentPath, csvdat);

                try
                {
                    if (items.Count > 0)
                    {
                        if (_dbService.GetDemoVersion())
                        {
                            if (_dbService.GetDemoAnzahl("3") == 5)
                            {
                                Config item = new Config() { ConfigID = "3", Bezeichnung = "DemoAnzInventur", Value = "1" };
                                _dbService.UpdateRow(item);
                            }
                        }

                        StreamWriter sr = new StreamWriter(filename, true, System.Text.Encoding.UTF8);

                        foreach (Inventur item in items)
                        {
                            string strdat = item.Positionsnummer + ";" + item.Artikelnummer + ";" + item.Menge.ToString() + ";" + item.Benutzer + ";" + item.Lager + ";" + item.Bereich + ";" + item.Lagerplatz;
                            sr.WriteLine(strdat);
                            dbService.DeleteRow(item);
                        }
                        sr.Close();
                        sr.Dispose();
                    }
                }
                catch (System.Exception ex)
                {
                    string meldung = ex.Message;
                }
            }
            private void WriteBestellerfassung()
            {
                DataAccessService dbService = new DataAccessService();

                List<Bestellerfassung> items = dbService.QueryAllBestellerfassung();
                string csvdat = "bestellerfassung_" + geraetename + "_" + Datum + ".csv";
                string filename = Path.Combine(documentPath, csvdat);

                try
                {
                    if (items.Count > 0)
                    {
                        if (_dbService.GetDemoVersion())
                        {
                            if (_dbService.GetDemoAnzahl("5") == 5)
                            {
                                Config item = new Config() { ConfigID = "5", Bezeichnung = "DemoAnzBestellerfassung", Value = "1" };
                                _dbService.UpdateRow(item);
                            }
                        }
                        StreamWriter sr = new StreamWriter(filename, true, System.Text.Encoding.UTF8);

                        foreach (Bestellerfassung item in items)
                        {
                            string strdat = item.Positionsnummer + ";" + item.Artikelnummer + ";" + item.Menge.ToString() + ";" + item.Benutzer + ";" + item.Lager + ";" + item.Bereich + ";" + item.Groesse + ";" + item.Farbe;
                            sr.WriteLine(strdat);
                            dbService.DeleteRow(item);
                        }
                        sr.Close();
                        sr.Dispose();
                    }
                }
                catch (System.Exception ex)
                {
                    string meldung = ex.Message;
                }
            }

        }

    }
}