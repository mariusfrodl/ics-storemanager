﻿using System;
using Autofac;
using ICS_Storemanager.Bootstrap;
using ICS_Storemanager.Droid.Services;
using abmsoft.Mobile.Core.Interfaces;
using abmsoft.Mobile.Core.Services;


namespace ICS_Storemanager.Droid
{
	public class DroidBootstrapper  : Bootstrapper
	{
		public DroidBootstrapper(Xamarin.Forms.Application application) : base(application)
		{
       }


        protected override void ConfigureContainer(ContainerBuilder builder)
		{
			base.ConfigureContainer(builder);

            // Register platform specific types
            //			builder.RegisterType<AzureBackendService> ().As<IBackendService> ().SingleInstance ();
        }
    }
}

