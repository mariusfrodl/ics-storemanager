﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using abmsoft.mobile.core.Layouts;
using System.Timers;
using ICS_Storemanager.Droid.Renderers;
using System.ComponentModel;
using Android.Views;
using System.Reflection;
using Java.Lang;
using Android.Graphics;
using Android.Widget;
using Android.Views.InputMethods;
using Android.OS;
using Android.Content.Res;

[assembly: ExportRenderer(typeof(SIPDisabledEntry), typeof(SIPDisabledEntryRenderer))]
namespace ICS_Storemanager.Droid.Renderers
{
    public class SIPDisabledEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null && e.NewElement != null)
            {
                var entry = (Xamarin.Forms.Entry)e.NewElement;
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                    Control.BackgroundTintList = ColorStateList.ValueOf(entry.TextColor.ToAndroid());
                else
                    Control.Background.SetColorFilter(entry.TextColor.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "TextColor")
            {
                var entry = (Xamarin.Forms.Entry)sender;
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                    Control.BackgroundTintList = ColorStateList.ValueOf(entry.TextColor.ToAndroid());
                else
                    Control.Background.SetColorFilter(entry.TextColor.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }
        }
    }

}