﻿using abmsoft.mobile.core.Layouts;
using Android.Graphics;
using Android.Widget;
using ICS_Storemanager.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Color = Android.Graphics.Color;

[assembly: ExportRenderer(typeof(OwnSwitchColor), typeof(OwnSwitchColorRenderer))]
namespace ICS_Storemanager.Droid.Renderers
{
    public class OwnSwitchColorRenderer : SwitchRenderer
    {
        private OwnSwitchColor view;

        private Color icsColor = Color.ParseColor("#2bbae7");
        // private Color greenColor = new Color(32, 156, 68);

        protected override void Dispose(bool disposing)
        {
            this.Control.CheckedChange -= this.OnCheckedChange;
            base.Dispose(disposing);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Switch> e)
        {
            base.OnElementChanged(e);

            if (this.Control != null)
            {
                if (this.Control.Checked)
                {
                    
                    this.Control.ThumbDrawable.SetColorFilter(Color.ParseColor("#2bbae7"), PorterDuff.Mode.SrcAtop);
                   // this.Control.Background.SetColorFilter(Color.ParseColor("#d8eef7"), PorterDuff.Mode.SrcAtop);
                    this.Control.TrackDrawable.SetColorFilter(Color.ParseColor("#d8eef7"), PorterDuff.Mode.SrcAtop);
                    this.Control.TrackDrawable.SetColorFilter(Color.ParseColor("#a7a7a7"), PorterDuff.Mode.SrcAtop);
                }
                else
                {
                    this.Control.ThumbDrawable.SetColorFilter(Color.ParseColor("#505050"), PorterDuff.Mode.SrcAtop);
                   // this.Control.Background.SetColorFilter(Color.ParseColor("#a7a7a7"), PorterDuff.Mode.SrcAtop);
                    this.Control.TrackDrawable.SetColorFilter(Color.ParseColor("#a7a7a7"), PorterDuff.Mode.SrcAtop);
                }

                this.Control.CheckedChange += this.OnCheckedChange;
            }
        }

        private void OnCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            Element.IsToggled = Control.Checked;

            if (this.Control.Checked)
            {
                this.Control.ThumbDrawable.SetColorFilter(Color.ParseColor("#2bbae7"), PorterDuff.Mode.SrcAtop);
                // this.Control.Background.SetColorFilter(Color.ParseColor("#d8eef7"), PorterDuff.Mode.SrcAtop);
                this.Control.TrackDrawable.SetColorFilter(Color.ParseColor("#d8eef7"), PorterDuff.Mode.SrcAtop);
                this.Control.TrackDrawable.SetColorFilter(Color.ParseColor("#a7a7a7"), PorterDuff.Mode.SrcAtop);
            }
            else
            {
                this.Control.ThumbDrawable.SetColorFilter(Color.ParseColor("#505050"), PorterDuff.Mode.SrcAtop);
                // this.Control.Background.SetColorFilter(Color.ParseColor("#a7a7a7"), PorterDuff.Mode.SrcAtop);
                this.Control.TrackDrawable.SetColorFilter(Color.ParseColor("#a7a7a7"), PorterDuff.Mode.SrcAtop);
            }
        }
    }
}
