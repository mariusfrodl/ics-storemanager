﻿using System;
using Autofac;
using Xamarin.Forms;
using abmsoft.Mobile.Core.Bootstrapping;
using abmsoft.Mobile.Core;
using abmsoft.Mobile.Core.Interfaces;
using System.Reflection;
using ICS_Storemanager.ViewModels;
using ICS_Storemanager;
using ICS_Storemanager.Views;

namespace ICS_Storemanager.Bootstrap
{
	public class Bootstrapper : AutofacBootstrapper
	{
		private readonly Application _application;

		public Bootstrapper(Application application)
		{
			_application = application;           
		}


		protected override void ConfigureContainer(ContainerBuilder builder)
		{
			base.ConfigureContainer(builder);

			builder.RegisterModule<AppModule>();

		} 

		protected override void RegisterViews(IViewFactory viewFactory)
		{

            viewFactory.Register<HauptmenueViewModel, Hauptmenue>();
            viewFactory.Register<MainViewModel, Main>();
            viewFactory.Register<InventurViewModel, InventurView>();
            viewFactory.Register<BestellerfassungViewModel, BestellerfassungView>();
            viewFactory.Register<WarenausgangViewModel, WarenausgangView>();
            viewFactory.Register<WareneingangViewModel, WareneingangView>();
            viewFactory.Register<EinstellungViewModel, EinstellungView>();
            viewFactory.Register<DatenuebertragungViewModel, DatenuebertragungView>();
        }

        protected override void ConfigureApplication(IContainer container)
		{
			// Set Current Culture
			//var localizeService = container.Resolve<ILocalize>();
			//Resources.Culture = localizeService.GetCurrentCultureInfo ();


			#if DEBUG
			//var assembly = typeof(App).GetTypeInfo().Assembly;
			//foreach (var res in assembly.GetManifestResourceNames())
			//	System.Diagnostics.Debug.WriteLine("found resource: " + res);
			#endif

			// set main page
			var viewFactory = container.Resolve<IViewFactory>();

            _application.MainPage = new NavigationPage(viewFactory.Resolve<MainViewModel>());

        }
	}

}

