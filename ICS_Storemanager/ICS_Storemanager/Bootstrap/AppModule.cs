﻿using System;
using Autofac;
using Xamarin.Forms;
using ICS_Storemanager.ViewModels;
using ICS_Storemanager.Views;
using ICS_Storemanager.Services;
using abmsoft.Mobile.Core.Services;
using abmsoft.Mobile.Core.Interfaces;
//using Plugin.Vibrate;
//using Plugin.Vibrate.Abstractions;
//using Plugin.TextToSpeech;
//using Plugin.TextToSpeech.Abstractions;


namespace ICS_Storemanager.Bootstrap
{
    public class AppModule : Module
	{
		public AppModule ()
		{
		}

		protected override void Load (ContainerBuilder builder)
		{



            #region Plugin registration


            //// Register Static instance of CrossConnectivity Plugin
            //builder.RegisterInstance(CrossConnectivity.Current).ExternallyOwned()
            //	.As<IConnectivity>();


            //// Register Static instance of CrossSettings plugin
            //builder.RegisterInstance(CrossSettings.Current).ExternallyOwned()
            //	.As<ISettings>();


            ////CrossDeviceInfo.Current
            //// Register Static instance of CrossDeviceInfo plugin
            //builder.RegisterInstance(CrossDeviceInfo.Current).ExternallyOwned()
            //	.As<IDeviceInfo>();


            //// Register Static instance of CrossSettings plugin
            //builder.RegisterInstance(CrossShare.Current).ExternallyOwned()
            //	.As<IShare>();


            //// Register Static instance of CrossConnectivity Plugin
            //builder.RegisterInstance(CrossConnectivity.Current).ExternallyOwned()
            //	.As<IConnectivity>();


            ////CrossDeviceMotion.Current
            //// Register Static instance of CrossDeviceMotion plugin
            //builder.RegisterInstance(CrossDeviceMotion.Current).ExternallyOwned()
            //	.As<IDeviceMotion>();


            //CrossVibrate.Current
            // Register Static instance of CrossVibrate plugin
            //builder.RegisterInstance(CrossVibrate.Current).ExternallyOwned()
            //    .As<IVibrate>();

            //builder.RegisterInstance(CrossTextToSpeech.Current).ExternallyOwned()
            //   .As<ITextToSpeech>();

            ////CrossMedia.Current
            //// Register Static instance of CrossMedia plugin
            //builder.RegisterInstance(CrossMedia.Current).ExternallyOwned()
            //	.As<IMedia>();



            #endregion

            #region "Registration Views/ViewModels"

            #region Services
            builder.RegisterType<MyService>().As<IMyService>().SingleInstance();

            #endregion

            builder.RegisterType<Main>();
            builder.RegisterType<MainViewModel>();
            builder.RegisterType<Hauptmenue>();
            builder.RegisterType<HauptmenueViewModel>();
            builder.RegisterType<InventurViewModel>();
            builder.RegisterType<InventurView>();
            builder.RegisterType<BestellerfassungViewModel>();
            builder.RegisterType<BestellerfassungView>();
            builder.RegisterType<WarenausgangViewModel>();
            builder.RegisterType<WarenausgangView>();
            builder.RegisterType<WareneingangViewModel>();
            builder.RegisterType<WareneingangView>();
            builder.RegisterType<EinstellungViewModel>();
            builder.RegisterType<EinstellungView>();
            builder.RegisterType<DatenuebertragungViewModel>();
            builder.RegisterType<DatenuebertragungView>();

            #endregion

            // override the default Page resolver of core
            builder.RegisterInstance<Func<Page>>(() => ((NavigationPage)Application.Current.MainPage).CurrentPage);

        }

    }
}