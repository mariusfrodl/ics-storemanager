﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ICS_Storemanager.Views
{
    public partial class WarenausgangView : ContentPage
    {
        public WarenausgangView()
        {
            InitializeComponent();
            this.Appearing += WarenausgangView_Appearing;
            this.ArtikelEntry.Completed += WarenausgangView_Artikel_Completed;
            this.LidEntry.Completed += WarenausgangView_Lid_Completed;
            this.StueckEntry.Completed += WarenausgangView_Stueck_Completed;
        }
        private void WarenausgangView_Appearing(object sender, EventArgs e)
        {
        }
        private void WarenausgangView_Artikel_Completed(object sender, EventArgs e)
        {
            if (LidEntry.IsVisible)
            {
                LidEntry.Focus();
            }
        }
        private void WarenausgangView_Lid_Completed(object sender, EventArgs e)
        {
            if (StueckEntry.IsVisible)
            {
                StueckEntry.Focus();
            }
        }
        private void WarenausgangView_Stueck_Completed(object sender, EventArgs e)
        {
            if (ArtikelEntry.IsVisible)
            {
                ArtikelEntry.Focus();
            }
        }
    }
}
