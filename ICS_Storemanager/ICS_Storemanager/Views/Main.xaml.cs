﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ICS_Storemanager.Views
{
    public partial class Main : ContentPage
    {
         public Main()
        {
            InitializeComponent();
            this.Appearing += Main_Appearing;
            this.BenutzerEntry.Completed += Main_Benutzer_Completed;
            this.LagerEntry.Completed += Main_Lager_Completed;
            this.BereichEntry.Completed += Main_Bereich_Completed;
        }

        private void Main_Appearing(object sender, EventArgs e)
        {
        }

        private void Main_Benutzer_Completed(object sender, EventArgs e)
        {
            if (LagerEntry.IsVisible)
            {
                LagerEntry.Focus();
            }
        }
        private void Main_Lager_Completed(object sender, EventArgs e)
        {
            if (BereichEntry.IsVisible)
            {
                BereichEntry.Focus();
            }
        }
        private void Main_Bereich_Completed(object sender, EventArgs e)
        {
        }
    }
}
