﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ICS_Storemanager.Views
{
    public partial class BestellerfassungView : ContentPage
    {
        public BestellerfassungView()
        {
            InitializeComponent();
            this.Appearing += BestellerfassungView_Appearing;
            this.ArtikelEntry.Completed += BestellerfassungView_Artikel_Completed;
            this.StueckEntry.Completed += BestellerfassungView_Stueck_Completed;
            this.GroesseEntry.Completed += BestellerfassungView_Groesse_Completed;
            this.FarbeEntry.Completed += BestellerfassungView_Farbe_Completed;
        }
        private void BestellerfassungView_Appearing(object sender, EventArgs e)
        {
        }
        private void BestellerfassungView_Artikel_Completed(object sender, EventArgs e)
        {
            if (GroesseEntry.IsVisible)
            {
                GroesseEntry.Focus();
            }
        }
        private void BestellerfassungView_Groesse_Completed(object sender, EventArgs e)
        {
            if (FarbeEntry.IsVisible)
            {
                FarbeEntry.Focus();
            }
        }
        private void BestellerfassungView_Farbe_Completed(object sender, EventArgs e)
        {
            if (StueckEntry.IsVisible)
            {
                StueckEntry.Focus();
            }
        }
        private void BestellerfassungView_Stueck_Completed(object sender, EventArgs e)
        {
            if (ArtikelEntry.IsVisible)
            {
                ArtikelEntry.Focus();
            }
        }
    }
}
