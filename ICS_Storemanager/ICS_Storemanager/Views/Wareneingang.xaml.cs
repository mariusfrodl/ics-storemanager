﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ICS_Storemanager.Views
{
    public partial class WareneingangView : ContentPage
    {
        public WareneingangView()
        {
            InitializeComponent();
            this.BackgroundColor = Color.White;

            this.Appearing += WareneingangView_Appearing;
            this.LieferscheinEntry.Completed += WareneingangView_Lieferschein_Completed;
            this.ArtikelEntry.Completed += WareneingangView_Artikel_Completed;
            this.LidEntry.Completed += WareneingangView_Lid_Completed;
            this.StueckEntry.Completed += WareneingangView_Stueck_Completed;
        }

        private void WareneingangView_Appearing(object sender, EventArgs e)
        {
        }

        private void WareneingangView_Lieferschein_Completed(object sender, EventArgs e)
        {
            if (ArtikelEntry.IsVisible)
            {
                ArtikelEntry.Focus();
            }
        }
        private void WareneingangView_Artikel_Completed(object sender, EventArgs e)
        {
            if (LidEntry.IsVisible)
            {
                LidEntry.Focus();
            }
        }
        private void WareneingangView_Lid_Completed(object sender, EventArgs e)
        {
            if (StueckEntry.IsVisible)
            {
                StueckEntry.Focus();
            }
        }
        private void WareneingangView_Stueck_Completed(object sender, EventArgs e)
        {
            if (LieferscheinEntry.IsVisible)
            {
                LieferscheinEntry.Focus();
            }
        }

    }
}
