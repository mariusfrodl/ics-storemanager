﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ICS_Storemanager.Views
{
    public partial class InventurView : ContentPage
    {
        public InventurView()
        {
            InitializeComponent();
            this.Appearing += InventurView_Appearing;
            this.ArtikelEntry.Completed += InventurView_Artikel_Completed;
            this.LidEntry.Completed += InventurView_Lid_Completed;
            this.StueckEntry.Completed += InventurView_Stueck_Completed;
        }
        private void InventurView_Appearing(object sender, EventArgs e)
        {
        }
        private void InventurView_Artikel_Completed(object sender, EventArgs e)
        {
            if (LidEntry.IsVisible)
            {
                LidEntry.Focus();
            }
        }
        private void InventurView_Lid_Completed(object sender, EventArgs e)
        {
            if (StueckEntry.IsVisible)
            {
                StueckEntry.Focus();
            }
        }
        private void InventurView_Stueck_Completed(object sender, EventArgs e)
        {
            if (ArtikelEntry.IsVisible)
            {
                //ArtikelEntry.Focus();
            }
        }
    }
}
