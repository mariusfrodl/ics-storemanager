﻿using abmsoft.Mobile.Core.ViewModels;
using System;
using Xamarin.Forms;
using abmsoft.Mobile.Core.Interfaces;
using XLabs.Forms.Mvvm;
using ICS_Storemanager.Views;
using abmsoft.Mobile.Core.Models.Database;
using SQLite;
using System.Windows.Input;
using System.Collections.ObjectModel;
using abmsoft.Mobile.Core.Services;
using abmsoft.mobile.core.Interfaces;
using System.Collections.Generic;

namespace ICS_Storemanager.ViewModels
{
    [ViewType(typeof(EinstellungView))]
    class EinstellungViewModel : ViewModelBase
    {
        INavigator _navigationService;
        DataAccessService _dbService;

        public ICommand WeiterCommand { private set; get; }
        public ICommand SpeichernCommand { private set; get; }
        public ICommand UpdateDatenbankCommand { private set; get; }
        public ICommand TestDatenbankCommand { private set; get; }
        public ICommand EinstellungTexteCommand { get; set; }
        public ICommand EinstellungHauptmenueCommand { get; set; }
        public ICommand EinstellungGeraetCommand { get; set; }
        public ICommand ScanPasswortCommand { private set; get; }
        public ICommand ScanLizenzCommand { private set; get; }
        public ICommand ZurueckCommand { private set; get; }

        private ObservableCollection<EntryProperty> _ListAnmeldungEntryProperty;
        public ObservableCollection<EntryProperty> ListAnmeldungEntryProperty { get; set; }
        public ObservableCollection<EntryProperty> ListBestellerfassungEntryProperty { get; set; }
        public ObservableCollection<EntryProperty> ListWareneingangEntryProperty { get; set; }
        public ObservableCollection<EntryProperty> ListWarenausgangEntryProperty { get; set; }
        public ObservableCollection<EntryProperty> ListInventurEntryProperty { get; set; }
        public ObservableCollection<Prozesse> ListProzesse { get; set; }
        public ObservableCollection<InvSystem> ListInvSystem { get; set; }

        Func<string, EinstellungViewModel> _EinstellungViewModelFactory;
        public EinstellungViewModel(INavigator navigationService, Func<string, EinstellungViewModel> EinstellungViewModelFactory)
        {
            _navigationService = navigationService;
            _dbService = new DataAccessService();
            _EinstellungViewModelFactory = EinstellungViewModelFactory;

            ListAnmeldungEntryProperty = new ObservableCollection<EntryProperty>();
            ListBestellerfassungEntryProperty = new ObservableCollection<EntryProperty>();
            ListWareneingangEntryProperty = new ObservableCollection<EntryProperty>();
            ListWarenausgangEntryProperty = new ObservableCollection<EntryProperty>();
            ListInventurEntryProperty = new ObservableCollection<EntryProperty>();
            ListProzesse = new ObservableCollection<Prozesse>();
            ListInvSystem = new ObservableCollection<InvSystem>();

            TitleUntermenue = "Einstellungen";
            MeldungTextColor = "Red";

            PasswortVisibility = true;

            ButtonHauptmenueBackColor = "#606060";
            ButtonSystemBackColor = "#606060";
            ButtonTexteBackColor = "#606060";

            // UpdateDatenbankVisibility = true;
            EinstellungTexteVisibility = false;
            EinstellungHauptmenueVisibility = false;
            EinstellungGeraetVisibility = false;

            if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
            {
                PasswortBarcodeVisibility = false;
            }
            else
            {
                PasswortBarcodeVisibility = true;
            }

            UebernehmenVisibility = true;
            EntryPropertyVisibility = false;
            ProzesseVisibility = false;
            InvSystemVisibility = false;
            WeiterVisibility = true;
            SpeichernVisibility = false;

            Meldung = string.Empty;

            //UpdateDatenbankCommand = new Command(async () =>
            //{
            //    try
            //    {
            //        Meldung = "Datenbank wird gelöscht.";
            //        DependencyService.Get<IDatabaseConnection>().DropDatabase();

            //        Meldung = "Datenbank wird erstellt.";

            //        await _dbService.CreateDatabase();

            //        Meldung = "Datenbank erstellen abgeschlossen.";
            //    }
            //    catch (Exception ex)
            //    {
            //        Meldung = ex.Message;
            //    }
            //});

            EinstellungTexteCommand = new Command(() =>
            {
                try
                {
                    ButtonHauptmenueBackColor = "#606060";
                    ButtonSystemBackColor = "#606060";
                    ButtonTexteBackColor = "#2bbae7";

                    Meldung = string.Empty;
                    FillListAnmeldungEntryProperty();
                    FillListBestellerfassungEntryProperty();
                    FillListWareneingangEntryProperty();
                    FillListWarenausgangEntryProperty();
                    FillListInventurEntryProperty();
                    UebernehmenVisibility = true;
                    EntryPropertyVisibility = true;
                    ProzesseVisibility = false;
                    InvSystemVisibility = false;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            EinstellungHauptmenueCommand = new Command(() =>
            {
                try
                {
                    ZeigeHauptmenue();
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            EinstellungGeraetCommand = new Command(() =>
            {
                try
                {
                    ButtonHauptmenueBackColor = "#606060";
                    ButtonSystemBackColor = "#2bbae7";
                    ButtonTexteBackColor = "#606060";

                    Meldung = string.Empty;
                    FillListGeraet();
                    UebernehmenVisibility = true;
                    EntryPropertyVisibility = false;
                    ProzesseVisibility = false;
                    InvSystemVisibility = true;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            WeiterCommand = new Command(() =>
            {
                try
                {
                    string pw = _dbService.GetEinstellungPasswort();

                    if (_PasswortValue == pw)
                    {
                        Meldung = string.Empty;
                        UebernehmenVisibility = false;
                        PasswortVisibility = false;
                        PasswortBarcodeVisibility = false;
                        // UpdateDatenbankVisibility = true;
                        EinstellungTexteVisibility = true;
                        EinstellungHauptmenueVisibility = true;
                        EinstellungGeraetVisibility = true;
                        WeiterVisibility = false;
                        SpeichernVisibility = true;

                        ZeigeHauptmenue();
                    }
                    else
                    {
                        MeldungTextColor = "Red";
                        Meldung = "Passwort ist falsch.";
                    }
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            SpeichernCommand = new Command(() =>
            {
                try
                {
                    Meldung = "Speichern gestartet";
                    if (EinstellungGeraetVisibility)
                    {
                        foreach (InvSystem item in ListInvSystem)
                        {
                            switch (item.SystemID)
                            {
                                case "0":
                                case "1":
                                case "2":
                                case "3":
                                case "5":
                                    {
                                        _dbService.UpdateRow(item);
                                        break;
                                    }
                                case "6":
                                    {
                                        if (_dbService.CheckLicense())
                                        {
                                            item.Value = "0";
                                        }
                                        else
                                        {
                                            item.Value = "1";
                                        }

                                        _dbService.UpdateRow(item);

                                        break;
                                    }
                            }
                        }
                        FillListGeraet();
                    }
                    if (EinstellungHauptmenueVisibility)
                    {
                        foreach (Prozesse item in ListProzesse)
                        {
                            _dbService.UpdateRow(item);
                        }
                    }
                    if (EinstellungTexteVisibility)
                    {
                        foreach (EntryProperty item in ListAnmeldungEntryProperty)
                        {
                            _dbService.UpdateRow(item);
                        }
                        foreach (EntryProperty item in ListBestellerfassungEntryProperty)
                        {
                            _dbService.UpdateRow(item);
                        }
                        foreach (EntryProperty item in ListWareneingangEntryProperty)
                        {
                            _dbService.UpdateRow(item);
                        }
                        foreach (EntryProperty item in ListWarenausgangEntryProperty)
                        {
                            _dbService.UpdateRow(item);
                        }
                        foreach (EntryProperty item in ListInventurEntryProperty)
                        {
                            _dbService.UpdateRow(item);
                        }
                    }
                    MeldungTextColor = "#089e70";
                    Meldung = "Speichern erfolgreich";
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ScanPasswortCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    PasswortValue = result.Text;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ScanLizenzCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    foreach (InvSystem item in ListInvSystem)
                    {
                        if (item.SystemID == "5")
                        {
                            item.Value = result.Text;
                            _dbService.UpdateRow(item);
                        }

                        if (item.SystemID == "6")
                        {
                            if (_dbService.CheckLicense())
                            {
                                item.Value = "0";
                            }
                            else
                            {
                                item.Value = "1";
                            }

                            _dbService.UpdateRow(item);
                        }

                    }
                    FillListGeraet();
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ZurueckCommand = new Command(async () =>
            {
                try
                {
                    await _navigationService.PopAsync();
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });
        }

        private void FillListAnmeldungEntryProperty()
        {
            List<EntryProperty> items = _dbService.QueryAllEntryProperty("0");

            ListAnmeldungEntryProperty.Clear();

            foreach (EntryProperty item in items)
            {
                ListAnmeldungEntryProperty.Add(item);
            }
        }
        private void FillListBestellerfassungEntryProperty()
        {
            List<EntryProperty> items = _dbService.QueryAllEntryProperty("6");

            ListBestellerfassungEntryProperty.Clear();

            foreach (EntryProperty item in items)
            {
                ListBestellerfassungEntryProperty.Add(item);
            }
        }
        private void FillListWareneingangEntryProperty()
        {
            List<EntryProperty> items = _dbService.QueryAllEntryProperty("1");

            ListWareneingangEntryProperty.Clear();

            foreach (EntryProperty item in items)
            {
                ListWareneingangEntryProperty.Add(item);
            }
        }
        private void FillListWarenausgangEntryProperty()
        {
            List<EntryProperty> items = _dbService.QueryAllEntryProperty("2");

            ListWarenausgangEntryProperty.Clear();

            foreach (EntryProperty item in items)
            {
                ListWarenausgangEntryProperty.Add(item);
            }
        }
        private void FillListInventurEntryProperty()
        {
            List<EntryProperty> items = _dbService.QueryAllEntryProperty("3");

            ListInventurEntryProperty.Clear();

            foreach (EntryProperty item in items)
            {
                ListInventurEntryProperty.Add(item);
            }
        }
        private void FillListProzesse()
        {
            ListProzesse.Clear();

            foreach (Prozesse item in _dbService.QueryAllProzesse())
            {
                if ((item.Bezeichnung != "Anmeldung") && (item.Bezeichnung != "Datenübertragung"))
                {
                    ListProzesse.Add(item);
                }
            }

            ListProzesse[1].Visibility = false;
        }
        private void FillListGeraet()
        {
            ListInvSystem.Clear();

            foreach (InvSystem item in _dbService.QueryAllInvSystem())
            {
                if ((item.Bezeichnung != "Lizenz") && (item.Bezeichnung != "Seriennummer") && (item.Bezeichnung != "Demo"))
                {
                    ListInvSystem.Add(item);
                }
            }
        }

        private void InitDisplayFelder()
        {
            int height = App.screenHeight;
            int width = App.screenWidth;

            ObererRand = (float)(height * 0.057);
            Leerzeile = (float)(height * 0.065);
            Meldungzeile = (float)(height * 0.049);

            if (height >= 1200)
            {
                Schrift = 34;
                ListViewZeileSchriftgroesse = 24;
                ListViewZeilenhoehe = 34;
                Buttonzeile = (float)(height * 0.05);
                UntererRand = (float)(height * 0.057);
            }
            else if (height >= 600)
            {
                Schrift = 16;
                ListViewZeileSchriftgroesse = 12;
                ListViewZeilenhoehe = 22;
                Buttonzeile = (float)(height * 0.064);
                UntererRand = (float)(height * 0.097);
            }
            else if (height >= 500)
            {
                Schrift = 16;
                ListViewZeileSchriftgroesse = 12;
                ListViewZeilenhoehe = 22;
                ObererRand = (float)(height * 0.065);
                Buttonzeile = (float)(height * 0.064);
                UntererRand = (float)(height * 0.097);
            }
            else
            {
                Schrift = 14;
                ListViewZeileSchriftgroesse = 11;
                ListViewZeilenhoehe = 20;
                Buttonzeile = (float)(height * 0.064);
                UntererRand = (float)(height * 0.097);
            }

        }

        private void ZeigeHauptmenue()
        {
            ButtonHauptmenueBackColor = "#2bbae7";
            ButtonSystemBackColor = "#606060";
            ButtonTexteBackColor = "#606060";

            Meldung = string.Empty;
            FillListProzesse();
            UebernehmenVisibility = true;
            EntryPropertyVisibility = false;
            ProzesseVisibility = true;
            InvSystemVisibility = false;
        }
        private string _meldung;
        public string Meldung
        {
            get { return _meldung; }
            set
            {
                SetProperty(ref _meldung, value, "Meldung");
            }
        }

        private string _meldungtextcolor;
        public string MeldungTextColor
        {
            get { return _meldungtextcolor; }
            set
            {
                SetProperty(ref _meldungtextcolor, value, "MeldungTextColor");
            }
        }

        private string _TitleUntermenue;
        public string TitleUntermenue
        {
            get { return _TitleUntermenue; }
            set
            {
                SetProperty(ref _TitleUntermenue, value, "TitleUntermenue");
            }
        }

        private Command _PasswortCommand;
        public Command PasswortCommand
        {
            get
            {
                this._PasswortCommand = this._PasswortCommand ?? new Command<EventArgs>((e) =>
                {
                    try
                    {
                        string pw = _dbService.GetEinstellungPasswort();

                        if (_PasswortValue == pw)
                        {
                            Meldung = string.Empty;
                            UebernehmenVisibility = false;
                            PasswortVisibility = false;
                            PasswortBarcodeVisibility = false;
                            // UpdateDatenbankVisibility = true;
                            EinstellungTexteVisibility = true;
                            EinstellungHauptmenueVisibility = true;
                            EinstellungGeraetVisibility = true;
                            WeiterVisibility = false;
                            SpeichernVisibility = true;

                            ZeigeHauptmenue();
                        }
                        else
                        {
                            MeldungTextColor = "Red";
                            Meldung = "Passwort ist falsch.";
                        }
                    }
                    catch (Exception ex)
                    {
                        MeldungTextColor = "Red";
                        Meldung = ex.Message;
                    }

                });
                return this._PasswortCommand;
            }
        }

        private string _ButtonHauptmenueBackColor;
        public string ButtonHauptmenueBackColor
        {
            get { return _ButtonHauptmenueBackColor; }
            set
            {
                SetProperty(ref _ButtonHauptmenueBackColor, value, "ButtonHauptmenueBackColor");
            }
        }
        private string _ButtonTexteBackColor;
        public string ButtonTexteBackColor
        {
            get { return _ButtonTexteBackColor; }
            set
            {
                SetProperty(ref _ButtonTexteBackColor, value, "ButtonTexteBackColor");
            }
        }
        private string _ButtonSystemBackColor;
        public string ButtonSystemBackColor
        {
            get { return _ButtonSystemBackColor; }
            set
            {
                SetProperty(ref _ButtonSystemBackColor, value, "ButtonSystemBackColor");
            }
        }

        private string _PasswortValue;
        public string PasswortValue
        {
            get { return _PasswortValue; }
            set
            {
                SetProperty(ref _PasswortValue, value, "PasswortValue");
            }
        }

        private double _Schrift;
        public double Schrift
        {
            get { return _Schrift; }
            set
            {
                SetProperty(ref _Schrift, value, "Schrift");
            }
        }

        private float _ObererRand;
        public float ObererRand
        {
            get { return _ObererRand; }
            set
            {
                SetProperty(ref _ObererRand, value, "ObererRand");
            }
        }

        private float _Leerzeile;
        public float Leerzeile
        {
            get { return _Leerzeile; }
            set
            {
                SetProperty(ref _Leerzeile, value, "Leerzeile");
            }
        }
        private float _Meldungzeile;
        public float Meldungzeile
        {
            get { return _Meldungzeile; }
            set
            {
                SetProperty(ref _Meldungzeile, value, "Meldungzeile");
            }
        }

        private float _Buttonzeile;
        public float Buttonzeile
        {
            get { return _Buttonzeile; }
            set
            {
                SetProperty(ref _Buttonzeile, value, "Buttonzeile");
            }
        }

        private double _ListViewZeileSchriftgroesse;
        public double ListViewZeileSchriftgroesse
        {
            get { return _ListViewZeileSchriftgroesse; }
            set
            {
                SetProperty(ref _ListViewZeileSchriftgroesse, value, "ListViewZeileSchriftgroesse");
            }
        }

        private float _ListViewZeilenhoehe;
        public float ListViewZeilenhoehe
        {
            get { return _ListViewZeilenhoehe; }
            set
            {
                SetProperty(ref _ListViewZeilenhoehe, value, "ListViewZeilenhoehe");
            }
        }

        private float _UntererRand;
        public float UntererRand
        {
            get { return _UntererRand; }
            set
            {
                SetProperty(ref _UntererRand, value, "UntererRand");
            }
        }


        #region "IsVisible"

        private bool _UebernehmenVisibility;
        public bool UebernehmenVisibility
        {
            get { return _UebernehmenVisibility; }
            set
            {
                SetProperty(ref _UebernehmenVisibility, value, "UebernehmenVisibility");
            }
        }

        private bool _PasswortVisibility;
        public bool PasswortVisibility
        {
            get { return _PasswortVisibility; }
            set
            {
                SetProperty(ref _PasswortVisibility, value, "PasswortVisibility");
            }
        }

        private bool _UpdateDatenbankVisibility;
        public bool UpdateDatenbankVisibility
        {
            get { return _UpdateDatenbankVisibility; }
            set
            {
                SetProperty(ref _UpdateDatenbankVisibility, value, "UpdateDatenbankVisibility");
            }
        }

        private bool _EinstellungTexteVisibility;
        public bool EinstellungTexteVisibility
        {
            get { return _EinstellungTexteVisibility; }
            set
            {
                SetProperty(ref _EinstellungTexteVisibility, value, "EinstellungTexteVisibility");
            }
        }

        private bool _EinstellungHauptmenueVisibility;
        public bool EinstellungHauptmenueVisibility
        {
            get { return _EinstellungHauptmenueVisibility; }
            set
            {
                SetProperty(ref _EinstellungHauptmenueVisibility, value, "EinstellungHauptmenueVisibility");
            }
        }

        private bool _EinstellungGeraetVisibility;
        public bool EinstellungGeraetVisibility
        {
            get { return _EinstellungGeraetVisibility; }
            set
            {
                SetProperty(ref _EinstellungGeraetVisibility, value, "EinstellungGeraetVisibility");
            }
        }

        private bool _EntryPropertyVisibility;
        public bool EntryPropertyVisibility
        {
            get { return _EntryPropertyVisibility; }
            set
            {
                SetProperty(ref _EntryPropertyVisibility, value, "EntryPropertyVisibility");
            }
        }

        private bool _ProzesseVisibility;
        public bool ProzesseVisibility
        {
            get { return _ProzesseVisibility; }
            set
            {
                SetProperty(ref _ProzesseVisibility, value, "ProzesseVisibility");
            }
        }

        private bool _InvSystemVisibility;
        public bool InvSystemVisibility
        {
            get { return _InvSystemVisibility; }
            set
            {
                SetProperty(ref _InvSystemVisibility, value, "InvSystemVisibility");
            }
        }

        private bool _WeiterVisibility;
        public bool WeiterVisibility
        {
            get { return _WeiterVisibility; }
            set
            {
                SetProperty(ref _WeiterVisibility, value, "WeiterVisibility");
            }
        }

        private bool _SpeichernVisibility;
        public bool SpeichernVisibility
        {
            get { return _SpeichernVisibility; }
            set
            {
                SetProperty(ref _SpeichernVisibility, value, "SpeichernVisibility");
            }
        }
        private bool _PasswortBarcodeVisibility;
        public bool PasswortBarcodeVisibility
        {
            get { return _PasswortBarcodeVisibility; }
            set
            {
                SetProperty(ref _PasswortBarcodeVisibility, value, "PasswortBarcodeVisibility");
            }
        }

        #endregion

        private Command _appearingCommand;
        public Command AppearingCommand
        {
            get
            {
                this._appearingCommand = this._appearingCommand ?? new Command(() =>
                {
                    InitDisplayFelder();
                });
                return this._appearingCommand;
            }
        }
    }
}
