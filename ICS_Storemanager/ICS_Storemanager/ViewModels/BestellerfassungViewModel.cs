﻿using abmsoft.Mobile.Core.ViewModels;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using abmsoft.Mobile.Core.Interfaces;
using XLabs.Forms.Mvvm;
using ICS_Storemanager.Views;
using abmsoft.Mobile.Core.Models.Database;
using System.Collections.ObjectModel;
using abmsoft.Mobile.Core.Services;
using abmsoft.mobile.core.Interfaces;
using SQLite;

namespace ICS_Storemanager.ViewModels
{
    [ViewType(typeof(BestellerfassungView))]
    class BestellerfassungViewModel : ViewModelBase
    {
        INavigator _navigationService;
        DataAccessService _dbService;
        public ObservableCollection<EntryProperty> ListEntryProperty { get; set; }
        public ObservableCollection<Bestellerfassung> ListBestellungen { get; set; }

        public System.Windows.Input.ICommand SpeichernCommand { private set; get; }
        public System.Windows.Input.ICommand LoeschenCommand { private set; get; }
        public System.Windows.Input.ICommand NeuCommand { private set; get; }
        public System.Windows.Input.ICommand ScanGroesseCommand { private set; get; }
        public System.Windows.Input.ICommand ScanArtikelCommand { private set; get; }
        public System.Windows.Input.ICommand ScanFarbeCommand { private set; get; }
        public System.Windows.Input.ICommand ZurueckCommand { private set; get; }

        Func<string, BestellerfassungViewModel> _BestellerfassungViewModelFactory;
        public BestellerfassungViewModel(INavigator navigationService, Func<string, BestellerfassungViewModel> BestellerfassungViewModelFactory)
        {
            _navigationService = navigationService;
            _dbService = new DataAccessService();
            _BestellerfassungViewModelFactory = BestellerfassungViewModelFactory;
            ListEntryProperty = new ObservableCollection<EntryProperty>();
            ListBestellungen = new ObservableCollection<Bestellerfassung>();

            TitleUntermenue = "Bestellerfassung";
            MeldungTextColor = "Red";

            DemoValue = "Logimat 2020";

            FillListEntryProperty();
 
            SpeichernCommand = new Command(() =>
            {
                Save();
            });

            NeuCommand = new Command(() =>
            {
                try
                {
                    SelectedBE = null;
                    FillListBestellungen();
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            LoeschenCommand = new Command(async () =>
            {
                try
                {
                    if ( SelectedBE != null)
                    {
                        string result = await App.Current.MainPage.DisplayActionSheet("Wollen Sie wirklich den Datensatz löschen?", "Nein", "Ja");

                        if (result == "Ja")
                        {
                            _dbService.DeleteRow(SelectedBE);
                            SelectedBE = null;
                            FillListBestellungen();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

             ScanGroesseCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    GroesseValue = result.Text;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ScanArtikelCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    ArtikelValue = result.Text;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ScanFarbeCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    FarbeValue = result.Text;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ZurueckCommand = new Command(async () =>
            {
                try
                {
                    await _navigationService.PopAsync();
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });
        }

        private async void Messagebox(string title)
        {
            await App.Current.MainPage.DisplayActionSheet(title, "Ja", "Nein");
        }

        public bool IsNumeric(string input)
        {
            int test;
            return int.TryParse(input, out test);
        }

        private  void FillListBestellungen()
        {
            try
            {
                List<Bestellerfassung> items =  _dbService.QueryAllBestellerfassung();

                ListBestellungen.Clear();

                foreach (Bestellerfassung item in items)
                {
                    item.Schriftgroesse = ListViewZeileSchriftgroesse;

                    ListBestellungen.Add(item);
                }

            }
            catch (Exception ex)
            {
                MeldungTextColor = "Red";
                Meldung = ex.Message;
            }
        }

        private  void FillListEntryProperty()
        {
            try
            {
                    List<EntryProperty> items = _dbService.QueryAllEntryProperty("6");

                    ListEntryProperty.Clear();

                    foreach (EntryProperty item in items)
                    {
                        ListEntryProperty.Add(item);
                    }

                    if (ListEntryProperty.Count > 0)
                    {
                        InitDisplay();
                    }
            }
            catch (Exception ex)
            {
                MeldungTextColor = "Red";
                Meldung = ex.Message;
            }
        }

        private void InitDisplayFelder()
        {
            int height = App.screenHeight;
            int width = App.screenWidth;

            ObererRand = (float)(height * 0.057);
            Leerzeile = (float)(height * 0.049);
            Eingabe1 = (float)(height * 0.065);
            Eingabe2 = (float)(height * 0.065);
            Eingabe3 = (float)(height * 0.065);
            Eingabe4 = (float)(height * 0.065);
            Meldungzeile = (float)(height * 0.049);

            if (height >= 1200)
            {
                Schrift = 34;
                ListViewZeileSchriftgroesse = 24;
                ListViewZeilenhoehe = 34;
                Buttonzeile = (float)(height * 0.05);
                UntererRand = (float)(height * 0.057);
            }
            else
            {
                Schrift = 16;
                ListViewZeileSchriftgroesse = 12;
                ListViewZeilenhoehe = 22;
                Buttonzeile = (float)(height * 0.064);
                UntererRand = (float)(height * 0.097);
            }

        }

        private void InitDisplay()
        {
            foreach (EntryProperty item in ListEntryProperty)
            {
                switch (item.FeldID)
                {
                    case "0":
                        ArtikelVisibility = item.Visibility;
                        ArtikelPlaceholder = item.Bezeichnung;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            ArtikelBarcodeVisibility = false;
                        }
                        else
                        {
                            ArtikelBarcodeVisibility = item.Visibility;
                        }
                        break;
                    case "1":
                        StueckVisibility = item.Visibility;
                        StueckPlaceholder = item.Bezeichnung;
                        StueckBarcodeVisibility = false;
                        break;
                    case "2":
                        GroesseVisibility = item.Visibility;
                        GroessePlaceholder = item.Bezeichnung;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            GroesseBarcodeVisibility = false;
                        }
                        else
                        {
                            GroesseBarcodeVisibility = item.Visibility;
                        }
                        break;
                    case "3":
                        FarbeVisibility = item.Visibility;
                        FarbePlaceholder = item.Bezeichnung;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            FarbeBarcodeVisibility = false;
                        }
                        else
                        {
                            FarbeBarcodeVisibility = item.Visibility;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private string _meldung;
        public string Meldung
        {
            get { return _meldung; }
            set
            {
                SetProperty(ref _meldung, value, "Meldung");
            }
        }

        private double _Schrift;
        public double Schrift
        {
            get { return _Schrift; }
            set
            {
                SetProperty(ref _Schrift, value, "Schrift");
            }
        }

        private float _ObererRand;
        public float ObererRand
        {
            get { return _ObererRand; }
            set
            {
                SetProperty(ref _ObererRand, value, "ObererRand");
            }
        }

        private float _Leerzeile;
        public float Leerzeile
        {
            get { return _Leerzeile; }
            set
            {
                SetProperty(ref _Leerzeile, value, "Leerzeile");
            }
        }

        private float _Eingabe1;
        public float Eingabe1
        {
            get { return _Eingabe1; }
            set
            {
                SetProperty(ref _Eingabe1, value, "Eingabe1");
            }
        }

        private float _Eingabe2;
        public float Eingabe2
        {
            get { return _Eingabe2; }
            set
            {
                SetProperty(ref _Eingabe2, value, "Eingabe2");
            }
        }

        private float _Eingabe3;
        public float Eingabe3
        {
            get { return _Eingabe3; }
            set
            {
                SetProperty(ref _Eingabe3, value, "Eingabe3");
            }
        }

        private float _Eingabe4;
        public float Eingabe4
        {
            get { return _Eingabe4; }
            set
            {
                SetProperty(ref _Eingabe4, value, "Eingabe4");
            }
        }

        private float _Meldungzeile;
        public float Meldungzeile
        {
            get { return _Meldungzeile; }
            set
            {
                SetProperty(ref _Meldungzeile, value, "Meldungzeile");
            }
        }

        private float _Buttonzeile;
        public float Buttonzeile
        {
            get { return _Buttonzeile; }
            set
            {
                SetProperty(ref _Buttonzeile, value, "Buttonzeile");
            }
        }

        private double _ListViewZeileSchriftgroesse;
        public double ListViewZeileSchriftgroesse
        {
            get { return _ListViewZeileSchriftgroesse; }
            set
            {
                SetProperty(ref _ListViewZeileSchriftgroesse, value, "ListViewZeileSchriftgroesse");
            }
        }

        private float _ListViewZeilenhoehe;
        public float ListViewZeilenhoehe
        {
            get { return _ListViewZeilenhoehe; }
            set
            {
                SetProperty(ref _ListViewZeilenhoehe, value, "ListViewZeilenhoehe");
            }
        }

        private float _UntererRand;
        public float UntererRand
        {
            get { return _UntererRand; }
            set
            {
                SetProperty(ref _UntererRand, value, "UntererRand");
            }
        }

        private string _meldungtextcolor;
        public string MeldungTextColor
        {
            get { return _meldungtextcolor; }
            set
            {
                SetProperty(ref _meldungtextcolor, value, "MeldungTextColor");
            }
        }
        private string _TitleUntermenue;
        public string TitleUntermenue
        {
            get { return _TitleUntermenue; }
            set
            {
                SetProperty(ref _TitleUntermenue, value, "TitleUntermenue");
            }
        }

        private string _Artikelnummer;
        public string ArtikelValue
        {
            get { return _Artikelnummer; }
            set
            {
                SetProperty(ref _Artikelnummer, value);
            }
        }

        private string _Groesse;
        public string GroesseValue
        {
            get { return _Groesse; }
            set
            {
                SetProperty(ref _Groesse, value);
            }
        }

        private string _strMenge;
        public string StueckValue
        {
            get { return _strMenge; }
            set
            {
                SetProperty(ref _strMenge, value);
            }
        }

        private string _Farbe;
        public string FarbeValue
        {
            get { return _Farbe; }
            set
            {
                SetProperty(ref _Farbe, value);
            }
        }

        private string _PickerSelItem;
        public string PickerSelItem
        {
            get { return _PickerSelItem; }
            set
            {
                SetProperty(ref _PickerSelItem, value);
            }
        }

        private string _FarbePlaceholder;
        public string FarbePlaceholder
        {
            get { return _FarbePlaceholder; }
            set
            {
                SetProperty(ref _FarbePlaceholder, value, "FarbePlaceholder");
            }
        }

        private string _ArtikelPlaceholder;
        public string ArtikelPlaceholder
        {
            get { return _ArtikelPlaceholder; }
            set
            {
                SetProperty(ref _ArtikelPlaceholder, value, "ArtikelPlaceholder");
            }
        }

        private string _StueckPlaceholder;
        public string StueckPlaceholder
        {
            get { return _StueckPlaceholder; }
            set
            {
                SetProperty(ref _StueckPlaceholder, value, "StueckPlaceholder");
            }
        }

        private string _GroessePlaceholder;
        public string GroessePlaceholder
        {
            get { return _GroessePlaceholder; }
            set
            {
                SetProperty(ref _GroessePlaceholder, value, "GroessePlaceholder");
            }
        }

        private bool _GroesseVisibility;
        public bool GroesseVisibility
        {
            get { return _GroesseVisibility; }
            set
            {
                SetProperty(ref _GroesseVisibility, value, "GroesseVisibility");
            }
        }

        private bool _GroesseBarcodeVisibility;
        public bool GroesseBarcodeVisibility
        {
            get { return _GroesseBarcodeVisibility; }
            set
            {
                SetProperty(ref _GroesseBarcodeVisibility, value, "GroesseBarcodeVisibility");
            }
        }

        private bool _ArtikelVisibility;
        public bool ArtikelVisibility
        {
            get { return _ArtikelVisibility; }
            set
            {
                SetProperty(ref _ArtikelVisibility, value, "ArtikelVisibility");
            }
        }

        private bool _ArtikelBarcodeVisibility;
        public bool ArtikelBarcodeVisibility
        {
            get { return _ArtikelBarcodeVisibility; }
            set
            {
                SetProperty(ref _ArtikelBarcodeVisibility, value, "ArtikelBarcodeVisibility");
            }
        }

        private bool _StueckVisibility;
        public bool StueckVisibility
        {
            get { return _StueckVisibility; }
            set
            {
                SetProperty(ref _StueckVisibility, value, "StueckVisibility");
            }
        }

        private bool _StueckBarcodeVisibility;
        public bool StueckBarcodeVisibility
        {
            get { return _StueckBarcodeVisibility; }
            set
            {
                SetProperty(ref _StueckBarcodeVisibility, value, "StueckBarcodeVisibility");
            }
        }

        private string _DemoValue;
        public string DemoValue
        {
            get { return _DemoValue; }
            set
            {
                SetProperty(ref _DemoValue, value, "DemoValue");
            }
        }

        private bool _FarbeVisibility;
        public bool FarbeVisibility
        {
            get { return _FarbeVisibility; }
            set
            {
                SetProperty(ref _FarbeVisibility, value, "FarbeVisibility");
            }
        }

        private bool _FarbeBarcodeVisibility;
        public bool FarbeBarcodeVisibility
        {
            get { return _FarbeBarcodeVisibility; }
            set
            {
                SetProperty(ref _FarbeBarcodeVisibility, value, "FarbeBarcodeVisibility");
            }
        }

        private Bestellerfassung valSelectedBE;

        public Bestellerfassung SelectedBE
        {
            get
            {
                return valSelectedBE;
            }
            set
            {
                if (value != valSelectedBE)
                {
                    valSelectedBE = value;

                    if (value != null)
                    {
                        ArtikelValue = value.Artikelnummer;
                        StueckValue = value.Menge.ToString();
                        GroesseValue = value.Groesse;
                        FarbeValue = value.Farbe;
                    }
                    else
                    {
                        ArtikelValue = string.Empty;
                        StueckValue = string.Empty;
                        GroesseValue = string.Empty;
                        FarbeValue = string.Empty;
                    }
                }
            }
        }

        private void Save()
        {
            try
            {
                Meldung = string.Empty;
                int anz = _dbService.QueryAllBestellerfassung().Count;

                if (anz >= 5)
                {
                    MeldungTextColor = "Red";
                    Meldung = "Demo-Version: Anzahl erreicht.";
                    return;
                }
                Bestellerfassung row = new Bestellerfassung();

                if (ArtikelVisibility && (string.IsNullOrEmpty(_Artikelnummer)))
                {
                    MeldungTextColor = "Red";
                    Meldung = ArtikelPlaceholder + " eingeben";
                    return;
                }
                if (StueckVisibility && (string.IsNullOrEmpty(_strMenge)))
                {
                    MeldungTextColor = "Red";
                    Meldung = StueckPlaceholder + " eingeben";
                    return;
                }
                else
                {
                    if (!(IsNumeric(_strMenge)))
                    {
                        MeldungTextColor = "Red";
                        Meldung = StueckPlaceholder + " als Zahl eingeben";
                        return;
                    }
                }
                if (GroesseVisibility && (string.IsNullOrEmpty(_Groesse)))
                {
                    MeldungTextColor = "Red";
                    Meldung = GroessePlaceholder + " eingeben";
                    return;
                }
                if (FarbeVisibility && (string.IsNullOrEmpty(_Farbe)))
                {
                    MeldungTextColor = "Red";
                    Meldung = FarbePlaceholder + " eingeben";
                    return;
                }

                row.Benutzer = GlobalData.Global.Benutzer;
                row.Artikelnummer = _Artikelnummer;
                row.Groesse = _Groesse;
                row.Farbe = _Farbe;
                row.Lager = GlobalData.Global.Lager;
                row.Bereich = GlobalData.Global.Bereich;
                row.Datum = DateTime.Now.ToString();

                if (IsNumeric(_strMenge))
                {
                    row.Menge = Convert.ToInt16(_strMenge);
                }
                else
                {
                    row.Menge = 0;
                }

                if (SelectedBE == null)
                {
                    _dbService.InsertRow(row);
                }
                else
                {
                    row.Positionsnummer = SelectedBE.Positionsnummer;
                    _dbService.UpdateRow(row);
                }

                ArtikelValue = string.Empty;
                StueckValue = string.Empty;
                GroesseValue = string.Empty;
                FarbeValue = string.Empty;
                MeldungTextColor = "#089e70";
                Meldung = "Speichern erfolgreich";

                FillListBestellungen();
                SelectedBE = null;
            }
            catch (Exception ex)
            {
                Meldung = ex.Message;
            }
        }

        private Command _ArtikelEntryCommand;
        public Command ArtikelEntryCommand
        {
            get
            {
                this._ArtikelEntryCommand = this._ArtikelEntryCommand ?? new Command<EventArgs>((e) =>
                {
                });
                return this._ArtikelEntryCommand;
            }
        }
        private Command _StueckEntryCommand;
        public Command StueckEntryCommand
        {
            get
            {
                this._StueckEntryCommand = this._StueckEntryCommand ?? new Command<EventArgs>((e) =>
                {
                    Save();
                });
                return this._StueckEntryCommand;
            }
        }
        private Command _GroesseEntryCommand;
        public Command GroesseEntryCommand
        {
            get
            {
                this._GroesseEntryCommand = this._GroesseEntryCommand ?? new Command<EventArgs>((e) =>
                {
                });
                return this._GroesseEntryCommand;
            }
        }
        private Command _FarbeEntryCommand;
        public Command FarbeEntryCommand
        {
            get
            {
                this._FarbeEntryCommand = this._FarbeEntryCommand ?? new Command<EventArgs>((e) =>
                {
                });
                return this._FarbeEntryCommand;
            }
        }

        private Command _appearingCommand;
        public Command AppearingCommand
        {
            get
            {
                this._appearingCommand = this._appearingCommand ?? new Command(() =>
                {
                    if (_dbService.GetDemoVersion())
                    {
                        DemoValue = "Logimat 2020";
                    }
                    else
                    {
                        DemoValue = string.Empty;
                    }
                    InitDisplayFelder();
                    FillListBestellungen();
                });
                return this._appearingCommand;
            }
        }
    }
}
