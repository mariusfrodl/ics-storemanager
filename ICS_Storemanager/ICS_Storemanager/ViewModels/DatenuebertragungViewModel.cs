﻿using abmsoft.Mobile.Core.ViewModels;
using System;
using Xamarin.Forms;
using abmsoft.Mobile.Core.Interfaces;
using XLabs.Forms.Mvvm;
using ICS_Storemanager.Views;
using abmsoft.mobile.core.Interfaces;
using abmsoft.Mobile.Core.Services;
using abmsoft.Mobile.Core.Models.Database;

namespace ICS_Storemanager.ViewModels
{
    [ViewType(typeof(DatenuebertragungView))]
    class DatenuebertragungViewModel : ViewModelBase
    {
        INavigator _navigationService;
        DataAccessService _dbService;

        public System.Windows.Input.ICommand UebernehmenCommand { private set; get; }
        public System.Windows.Input.ICommand ZurueckCommand { private set; get; }

        Func<string, DatenuebertragungViewModel> _DatenuebertragungViewModelFactory;
        public DatenuebertragungViewModel(INavigator navigationService, Func<string, DatenuebertragungViewModel> DatenuebertragungViewModelFactory)
        {
            _navigationService = navigationService;
            _dbService = new DataAccessService();
            _DatenuebertragungViewModelFactory = DatenuebertragungViewModelFactory;

            TitleUntermenue = "Datenübertragung";

            if (_dbService.GetDemoVersion())
            {
                DemoValue = "Logimat 2020";
             }
            else
            {
                DemoValue = string.Empty;
            }

            Xamarin.Forms.DependencyService.Get<IDatabaseToFile>().AllDataTableToFile();

            Meldung = "Die CSV - Dateien wurden erzeugt und im Download - Verzeichnis unter ICS_Storemanager abgelegt.";

            ZurueckCommand = new Command(async () =>
            {
                try
                {
                    await _navigationService.PopAsync();
                }
                catch (Exception ex)
                {
                    Meldung = ex.Message;
                }
            });

        }

        private string _meldung;
        public string Meldung
        {
            get { return _meldung; }
            set
            {
                SetProperty(ref _meldung, value, "Meldung");
            }
        }

        private string _DemoValue;
        public string DemoValue
        {
            get { return _DemoValue; }
            set
            {
                SetProperty(ref _DemoValue, value, "DemoValue");
            }
        }

        private string _TitleUntermenue;
        public string TitleUntermenue
        {
            get { return _TitleUntermenue; }
            set
            {
                SetProperty(ref _TitleUntermenue, value, "TitleUntermenue");
            }
        }
    }
}
