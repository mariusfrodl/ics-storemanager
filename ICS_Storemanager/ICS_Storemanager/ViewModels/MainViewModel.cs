﻿using abmsoft.mobile.core.Interfaces;
using abmsoft.Mobile.Core.Interfaces;
using abmsoft.Mobile.Core.Models.Database;
using abmsoft.Mobile.Core.Services;
using abmsoft.Mobile.Core.ViewModels;
using ICS_Storemanager.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using XLabs.Forms.Mvvm;

namespace ICS_Storemanager.ViewModels
{
    [ViewType(typeof(Main))]
    class MainViewModel : ViewModelBase
    {
        INavigator _navigationService;
        DataAccessService _dbService;

        public ObservableCollection<EntryProperty> ListEntryProperty { get; set; }
        public System.Windows.Input.ICommand ScanBenutzerCommand { private set; get; }
        public System.Windows.Input.ICommand ScanLagerCommand { private set; get; }
        public System.Windows.Input.ICommand ScanBereichCommand { private set; get; }

        public MainViewModel(INavigator navigationService, Func<string, HauptmenueViewModel> MainViewModelFactory)
        {
            _navigationService = navigationService;
            _dbService = new DataAccessService();
            ListEntryProperty = new ObservableCollection<EntryProperty>();

            HeaderValue = "Anmeldung ICS_Storemanager";

            ScanBenutzerCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    BenutzerValue = result.Text;
                }
                catch (Exception ex)
                {
                    Meldung = ex.Message;
                }
            });

            ScanLagerCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    LagerValue = result.Text;
                }
                catch (Exception ex)
                {
                    Meldung = ex.Message;
                }
            });

            ScanBereichCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    BereichValue = result.Text;
                }
                catch (Exception ex)
                {
                    Meldung = ex.Message;
                }
            });


        }

        private void FillListEntryProperty()
        {
            try
            {
                if (DependencyService.Get<IDatabaseConnection>().ExistsDatabase())
                {
                    List<EntryProperty> items = _dbService.QueryAllEntryProperty("0");

                    ListEntryProperty.Clear();

                    foreach (EntryProperty item in items)
                    {
                        ListEntryProperty.Add(item);
                    }

                    if (ListEntryProperty.Count > 0)
                    {
                        InitDisplay();
                    }
                }
            }
            catch (Exception ex)
            {
                Meldung = ex.Message;
            }
        }

        private void InitDisplayFelder()
        {
            int height = App.screenHeight;
            int width = App.screenWidth;

            ObererRand = (float)(height * 0.049);
            ICS_Logo = (float)(height * 0.162);
            Leerzeile = (float)(height * 0.032);
            Benutzerzeile = (float)(height * 0.065);
            Lagerzeile = (float)(height * 0.065);
            Bereichzeile = (float)(height * 0.065);
            Wareneingangzeile = (float)(height * 0.049);
            Warenausgangzeile = (float)(height * 0.049);
            Inventurzeile = (float)(height * 0.049);
            Bestellungzeile = (float)(height * 0.049);
            Meldungzeile = (float)(height * 0.049);
            Buttonzeile = (float)(height * 0.065);
            UntererRand = (float)(height * 0.008);

            if (height >= 1200)
            {
                Schrift = 40;
            }
            else if (height >= 600)
            {
                Schrift = 16;
            }
            else if (height >= 500)
            {
                Schrift = 16;
                ICS_Logo = (float)(height * 0.11);
                Wareneingangzeile = (float)(height * 0.03);
                Warenausgangzeile = (float)(height * 0.03);
                Inventurzeile = (float)(height * 0.03);
                Bestellungzeile = (float)(height * 0.03);
                UntererRand = (float)(height * 0.02);
            }
            else
            {
                Schrift = 14;
                UntererRand = (float)(height * 0.01);
            }
        }

        private void InitDisplay()
        {
            foreach (EntryProperty item in ListEntryProperty)
            {
                switch (item.FeldID)
                {
                    case "0":
                        BenutzerVisibility = item.Visibility;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            BenutzerBarcodeVisibility = false;
                        }
                        else
                        {
                            BenutzerBarcodeVisibility = item.Visibility;
                        }
                        BenutzerPlaceholder = item.Bezeichnung;
                        break;
                    case "1":
                        LagerVisibility = item.Visibility;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            LagerBarcodeVisibility = false;
                        }
                        else
                        {
                            LagerBarcodeVisibility = item.Visibility;
                        }
                        LagerPlaceholder = item.Bezeichnung;
                        break;
                    case "2":
                        BereichVisibility = item.Visibility;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            BereichBarcodeVisibility = false;
                        }
                        else
                        {
                            BereichBarcodeVisibility = item.Visibility;
                        }
                        BereichPlaceholder = item.Bezeichnung;
                        break;
                    default:
                        break;
                }
            }
        }

        private void GetAnzahl()
        {
            try
            {
                PositionWEValue = _dbService.QueryAllWareneingang().Count.ToString();
                PositionWAValue = _dbService.QueryAllWarenausgang().Count.ToString();
                PositionINValue = _dbService.QueryAllInventur().Count.ToString();
                PositionBEValue = _dbService.QueryAllBestellerfassung().Count.ToString();
            }
            catch (Exception e)
            {
                string txt = e.Message;
            }
        }

        private Command _AnmeldenCommand;
        public Command AnmeldenCommand
        {
            get
            {
                this._AnmeldenCommand = new Command<HauptmenueViewModel>(p =>
                {
                    StartAPP();
                });
                return this._AnmeldenCommand;
            }
        }

        private void StartAPP()
        {
            if (BenutzerVisibility && (string.IsNullOrEmpty(_BenutzerValue)))
            {
                Meldung = "Benutzer eingeben";
                return;
            }

            if (LagerVisibility && (string.IsNullOrEmpty(_LagerValue)))
            {
                Meldung = "Lager eingeben";
                return;
            }

            if (BereichVisibility && (string.IsNullOrEmpty(_BereichValue)))
            {
                Meldung = "Bereich eingeben";
                return;
            }

            Meldung = string.Empty;

            _navigationService.PushAsync<HauptmenueViewModel>();
        }

        private string _BenutzerValue;
        public string BenutzerValue
        {
            get { return _BenutzerValue; }
            set
            {
                SetProperty(ref _BenutzerValue, value, "BenutzerValue");
                GlobalData.Global.Benutzer = _BenutzerValue;
            }
        }

        private string _meldung;
        public string Meldung
        {
            get { return _meldung; }
            set
            {
                SetProperty(ref _meldung, value, "Meldung");
            }
        }

        private double _Schrift;
        public double Schrift
        {
            get { return _Schrift; }
            set
            {
                SetProperty(ref _Schrift, value, "Schrift");
            }
        }

        private float _ObererRand;
        public float ObererRand
        {
            get { return _ObererRand; }
            set
            {
                SetProperty(ref _ObererRand, value, "ObererRand");
            }
        }

        private float _ICS_Logo;
        public float ICS_Logo
        {
            get { return _ICS_Logo; }
            set
            {
                SetProperty(ref _ICS_Logo, value, "ICS_Logo");
            }
        }
        private float _Leerzeile;
        public float Leerzeile
        {
            get { return _Leerzeile; }
            set
            {
                SetProperty(ref _Leerzeile, value, "Leerzeile");
            }
        }

        private float _Benutzerzeile;
        public float Benutzerzeile
        {
            get { return _Benutzerzeile; }
            set
            {
                SetProperty(ref _Benutzerzeile, value, "Benutzerzeile");
            }
        }

        private float _Lagerzeile;
        public float Lagerzeile
        {
            get { return _Lagerzeile; }
            set
            {
                SetProperty(ref _Lagerzeile, value, "Lagerzeile");
            }
        }

        private float _Bereichzeile;
        public float Bereichzeile
        {
            get { return _Bereichzeile; }
            set
            {
                SetProperty(ref _Bereichzeile, value, "Bereichzeile");
            }
        }

        private float _Wareneingangzeile;
        public float Wareneingangzeile
        {
            get { return _Wareneingangzeile; }
            set
            {
                SetProperty(ref _Wareneingangzeile, value, "Wareneingangzeile");
            }
        }

        private float _Warenausgangzeile;
        public float Warenausgangzeile
        {
            get { return _Warenausgangzeile; }
            set
            {
                SetProperty(ref _Warenausgangzeile, value, "Warenausgangzeile");
            }
        }

        private float _Inventurzeile;
        public float Inventurzeile
        {
            get { return _Inventurzeile; }
            set
            {
                SetProperty(ref _Inventurzeile, value, "Inventurzeile");
            }
        }

        private float _Bestellungzeile;
        public float Bestellungzeile
        {
            get { return _Bestellungzeile; }
            set
            {
                SetProperty(ref _Bestellungzeile, value, "Bestellungzeile");
            }
        }

        private float _Meldungzeile;
        public float Meldungzeile
        {
            get { return _Meldungzeile; }
            set
            {
                SetProperty(ref _Meldungzeile, value, "Meldungzeile");
            }
        }

        private float _Buttonzeile;
        public float Buttonzeile
        {
            get { return _Buttonzeile; }
            set
            {
                SetProperty(ref _Buttonzeile, value, "Buttonzeile");
            }
        }

        private float _UntererRand;
        public float UntererRand
        {
            get { return _UntererRand; }
            set
            {
                SetProperty(ref _UntererRand, value, "UntererRand");
            }
        }

        private string _LagerValue;
        public string LagerValue
        {
            get { return _LagerValue; }
            set
            {
                SetProperty(ref _LagerValue, value, "LagerValue");
                GlobalData.Global.Lager = _LagerValue;
            }
        }

        private string _BereichValue;
        public string BereichValue
        {
            get { return _BereichValue; }
            set
            {
                SetProperty(ref _BereichValue, value, "BereichValue");
                GlobalData.Global.Bereich = _BereichValue;
            }
        }

        private string _PositionWEValue;
        public string PositionWEValue
        {
            get { return _PositionWEValue; }
            set
            {
                SetProperty(ref _PositionWEValue, value, "PositionWEValue");
            }
        }

        private string _PositionWAValue;
        public string PositionWAValue
        {
            get { return _PositionWAValue; }
            set
            {
                SetProperty(ref _PositionWAValue, value, "PositionWAValue");
            }
        }

        private string _PositionINValue;
        public string PositionINValue
        {
            get { return _PositionINValue; }
            set
            {
                SetProperty(ref _PositionINValue, value, "PositionINValue");
            }
        }

        private string _PositionUMValue;
        public string PositionUMValue
        {
            get { return _PositionUMValue; }
            set
            {
                SetProperty(ref _PositionUMValue, value, "PositionUMValue");
            }
        }

        private string _PositionBEValue;
        public string PositionBEValue
        {
            get { return _PositionBEValue; }
            set
            {
                SetProperty(ref _PositionBEValue, value, "PositionBEValue");
            }
        }

        private string _BenutzerPlaceholder;
        public string BenutzerPlaceholder
        {
            get { return _BenutzerPlaceholder; }
            set
            {
                SetProperty(ref _BenutzerPlaceholder, value, "BenutzerPlaceholder");
            }
        }

        private string _LagerPlaceholder;
        public string LagerPlaceholder
        {
            get { return _LagerPlaceholder; }
            set
            {
                SetProperty(ref _LagerPlaceholder, value, "LagerPlaceholder");
            }
        }

        private string _BereichPlaceholder;
        public string BereichPlaceholder
        {
            get { return _BereichPlaceholder; }
            set
            {
                SetProperty(ref _BereichPlaceholder, value, "BereichPlaceholder");
            }
        }

        private bool _BenutzerVisibility;
        public bool BenutzerVisibility
        {
            get { return _BenutzerVisibility; }
            set
            {
                SetProperty(ref _BenutzerVisibility, value, "BenutzerVisibility");
            }
        }

        private bool _BenutzerBarcodeVisibility;
        public bool BenutzerBarcodeVisibility
        {
            get { return _BenutzerBarcodeVisibility; }
            set
            {
                SetProperty(ref _BenutzerBarcodeVisibility, value, "BenutzerBarcodeVisibility");
            }
        }

        private bool _LagerVisibility;
        public bool LagerVisibility
        {
            get { return _LagerVisibility; }
            set
            {
                SetProperty(ref _LagerVisibility, value, "LagerVisibility");
            }
        }

        private bool _LagerBarcodeVisibility;
        public bool LagerBarcodeVisibility
        {
            get { return _LagerBarcodeVisibility; }
            set
            {
                SetProperty(ref _LagerBarcodeVisibility, value, "LagerBarcodeVisibility");
            }
        }

        private bool _BereichVisibility;
        public bool BereichVisibility
        {
            get { return _BereichVisibility; }
            set
            {
                SetProperty(ref _BereichVisibility, value, "BereichVisibility");
            }
        }

        private bool _BereichBarcodeVisibility;
        public bool BereichBarcodeVisibility
        {
            get { return _BereichBarcodeVisibility; }
            set
            {
                SetProperty(ref _BereichBarcodeVisibility, value, "BereichBarcodeVisibility");
            }
        }

        private string _HeaderValue;
        public string HeaderValue
        {
            get { return _HeaderValue; }
            set
            {
                SetProperty(ref _HeaderValue, value, "HeaderValue");
            }
        }

        private Image _scanimage;
        public Image ScanImage
        {
            get { return _scanimage; }
            set
            {
                SetProperty(ref _scanimage, value, "ScanImage");
            }
        }

        private string _DemoValue;
        public string DemoValue
        {
            get { return _DemoValue; }
            set
            {
                SetProperty(ref _DemoValue, value, "DemoValue");
            }
        }

        private Command _appearingCommand;
        public Command AppearingCommand
        {
            get
            {
                this._appearingCommand = this._appearingCommand ?? new Command(() =>
                {
                    if (BenutzerVisibility)
                    {
                        BenutzerValue = "";
                    }

                    if (LagerVisibility)
                    {
                        LagerValue = "";
                    }

                    if (BereichVisibility)
                    {
                        BereichValue = "";
                    }

                    if (_dbService.GetDemoVersion())
                    {
                        DemoValue = "Logimat 2020";
                    }
                    else
                    {
                        DemoValue = string.Empty;
                    }
                    InitDisplayFelder();
                    GetAnzahl();
                    FillListEntryProperty();
                });
                return this._appearingCommand;
            }
        }

        private Command _BenutzerEnterCommand;
        public Command BenutzerEnterCommand
        {
            get
            {
                this._BenutzerEnterCommand = this._BenutzerEnterCommand ?? new Command<EventArgs>((e) =>
                {
                    if (!LagerVisibility && !BereichVisibility)
                    {
                        StartAPP();
                    }
                });
                return this._BenutzerEnterCommand;
            }
        }
        private Command _BereichEnterCommand;
        public Command BereichEnterCommand
        {
            get
            {
                this._BereichEnterCommand = this._BereichEnterCommand ?? new Command<EventArgs>((e) =>
                {
                    StartAPP();
                });
                return this._BereichEnterCommand;
            }
        }

        private Command _LagerEnterCommand;
        public Command LagerEnterCommand
        {
            get
            {
                this._LagerEnterCommand = this._LagerEnterCommand ?? new Command<EventArgs>((e) =>
                {
                    if (!BereichVisibility)
                    {
                        StartAPP();
                    }
                });
                return this._LagerEnterCommand;
            }
        }

    }

}

