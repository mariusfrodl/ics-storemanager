﻿using abmsoft.Mobile.Core.ViewModels;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using abmsoft.Mobile.Core.Interfaces;
using XLabs.Forms.Mvvm;
using ICS_Storemanager.Views;
using abmsoft.Mobile.Core.Models.Database;
using System.Collections.ObjectModel;
using abmsoft.Mobile.Core.Services;
using abmsoft.mobile.core.Interfaces;
using SQLite;
using System.Windows.Input;

namespace ICS_Storemanager.ViewModels
{
    [ViewType(typeof(WareneingangView))]
    class WareneingangViewModel : ViewModelBase
    {
        INavigator _navigationService;
        DataAccessService _dbService;
        public ObservableCollection<EntryProperty> ListEntryProperty { get; set; }
        public ObservableCollection<Wareneingang> ListWareneingaenge { get; set; }

        public ICommand SpeichernCommand { private set; get; }
        public ICommand LoeschenCommand { private set; get; }
        public ICommand NeuCommand { private set; get; }
        public ICommand ScanLieferscheinCommand { private set; get; }
        public ICommand ScanArtikelCommand { private set; get; }
        public ICommand ScanLidCommand { private set; get; }
        public ICommand ZurueckCommand { private set; get; }

        Func<string, WareneingangViewModel> _WareneingangViewModelFactory;
        public WareneingangViewModel(INavigator navigationService, Func<string, WareneingangViewModel> WareneingangViewModelFactory)
        {
            _navigationService = navigationService;
            _dbService = new DataAccessService();
            _WareneingangViewModelFactory = WareneingangViewModelFactory;
            ListEntryProperty = new ObservableCollection<EntryProperty>();
            ListWareneingaenge = new ObservableCollection<Wareneingang>();

            TitleUntermenue = "Wareneingang";
            MeldungTextColor = "Red";

            DemoValue = "Logimat 2020";

            SpeichernCommand = new Command(() =>
            {
                Save();
             });

            NeuCommand = new Command(() =>
            {
                try
                {
                    SelectedWE = null;
                    FillListWareneingaenge();
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            LoeschenCommand = new Command(async () =>
            {
                try
                {
                    if ( SelectedWE != null)
                    {
                        string result = await App.Current.MainPage.DisplayActionSheet("Wollen Sie wirklich den Datensatz löschen?", "Nein", "Ja");

                        if (result == "Ja")
                        {
                            _dbService.DeleteRow(SelectedWE);
                            SelectedWE = null;
                            FillListWareneingaenge();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

             ScanLieferscheinCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    LieferscheinValue = result.Text;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ScanArtikelCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    ArtikelValue = result.Text;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ScanLidCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    LidValue = result.Text;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ZurueckCommand = new Command(async () =>
            {
                try
                {
                    await _navigationService.PopAsync();
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });
        }

        private async void Messagebox(string title)
        {
            await App.Current.MainPage.DisplayActionSheet(title, "Ja", "Nein");
        }

        public bool IsNumeric(string input)
        {
            int test;
            return int.TryParse(input, out test);
        }

        private  void FillListWareneingaenge()
        {
            try
            {
                List<Wareneingang> items =  _dbService.QueryAllWareneingang();

                ListWareneingaenge.Clear();

                foreach (Wareneingang item in items)
                {
                    item.Schriftgroesse = ListViewZeileSchriftgroesse;

                    ListWareneingaenge.Add(item);
                }

            }
            catch (Exception ex)
            {
                MeldungTextColor = "Red";
                Meldung = ex.Message;
            }
        }

        private  void FillListEntryProperty()
        {
            try
            {
                    List<EntryProperty> items = _dbService.QueryAllEntryProperty("1");

                    ListEntryProperty.Clear();

                    foreach (EntryProperty item in items)
                    {
                        ListEntryProperty.Add(item);
                    }

                    if (ListEntryProperty.Count > 0)
                    {
                        InitDisplay();
                    }
            }
            catch (Exception ex)
            {
                MeldungTextColor = "Red";
                Meldung = ex.Message;
            }
        }

        private void InitDisplayFelder()
        {
            int height = App.screenHeight;
            int width = App.screenWidth;

            ObererRand = (float)(height * 0.057);
            Leerzeile = (float)(height * 0.049);
            Eingabe1 = (float)(height * 0.065);
            Eingabe2 = (float)(height * 0.065);
            Eingabe3 = (float)(height * 0.065);
            Eingabe4 = (float)(height * 0.065);
            Meldungzeile = (float)(height * 0.049);

            if (height >= 1200)
            {
                Schrift = 34;
                ListViewZeileSchriftgroesse = 24;
                ListViewZeilenhoehe = 34;
                Buttonzeile = (float)(height * 0.05);
                UntererRand = (float)(height * 0.057);
            }
            else
            {
                Schrift = 16;
                ListViewZeileSchriftgroesse = 12;
                ListViewZeilenhoehe = 22;
                Buttonzeile = (float)(height * 0.064);
                UntererRand = (float)(height * 0.097);
            }

        }

        private void InitDisplay()
        {
            foreach (EntryProperty item in ListEntryProperty)
            {
                switch (item.FeldID)
                {
                    case "0":
                        LieferscheinVisibility = item.Visibility;
                        LieferscheinPlaceholder = item.Bezeichnung;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            LieferscheinBarcodeVisibility = false;
                        }
                        else
                        {
                            LieferscheinBarcodeVisibility = item.Visibility;
                        }
                        break;
                    case "1":
                        ArtikelVisibility = item.Visibility;
                        ArtikelPlaceholder = item.Bezeichnung;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            ArtikelBarcodeVisibility = false;
                        }
                        else
                        {
                            ArtikelBarcodeVisibility = item.Visibility;
                        }
                        break;
                    case "2":
                        StueckVisibility = item.Visibility;
                        StueckPlaceholder = item.Bezeichnung;
                        StueckBarcodeVisibility = false;
                        break;
                    case "3":
                        LidVisibility = item.Visibility;
                        LidPlaceholder = item.Bezeichnung;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            LidBarcodeVisibility = false;
                        }
                        else
                        {
                            LidBarcodeVisibility = item.Visibility;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private string _meldung;
        public string Meldung
        {
            get { return _meldung; }
            set
            {
                SetProperty(ref _meldung, value, "Meldung");
            }
        }

        private double _Schrift;
        public double Schrift
        {
            get { return _Schrift; }
            set
            {
                SetProperty(ref _Schrift, value, "Schrift");
            }
        }

        private float _ObererRand;
        public float ObererRand
        {
            get { return _ObererRand; }
            set
            {
                SetProperty(ref _ObererRand, value, "ObererRand");
            }
        }

        private float _Leerzeile;
        public float Leerzeile
        {
            get { return _Leerzeile; }
            set
            {
                SetProperty(ref _Leerzeile, value, "Leerzeile");
            }
        }

        private float _Eingabe1;
        public float Eingabe1
        {
            get { return _Eingabe1; }
            set
            {
                SetProperty(ref _Eingabe1, value, "Eingabe1");
            }
        }

        private float _Eingabe2;
        public float Eingabe2
        {
            get { return _Eingabe2; }
            set
            {
                SetProperty(ref _Eingabe2, value, "Eingabe2");
            }
        }

        private float _Eingabe3;
        public float Eingabe3
        {
            get { return _Eingabe3; }
            set
            {
                SetProperty(ref _Eingabe3, value, "Eingabe3");
            }
        }

        private float _Eingabe4;
        public float Eingabe4
        {
            get { return _Eingabe4; }
            set
            {
                SetProperty(ref _Eingabe4, value, "Eingabe4");
            }
        }

        private float _Meldungzeile;
        public float Meldungzeile
        {
            get { return _Meldungzeile; }
            set
            {
                SetProperty(ref _Meldungzeile, value, "Meldungzeile");
            }
        }

        private float _Buttonzeile;
        public float Buttonzeile
        {
            get { return _Buttonzeile; }
            set
            {
                SetProperty(ref _Buttonzeile, value, "Buttonzeile");
            }
        }

        private double _ListViewZeileSchriftgroesse;
        public double ListViewZeileSchriftgroesse
        {
            get { return _ListViewZeileSchriftgroesse; }
            set
            {
                SetProperty(ref _ListViewZeileSchriftgroesse, value, "ListViewZeileSchriftgroesse");
            }
        }

        private float _ListViewZeilenhoehe;
        public float ListViewZeilenhoehe
        {
            get { return _ListViewZeilenhoehe; }
            set
            {
                SetProperty(ref _ListViewZeilenhoehe, value, "ListViewZeilenhoehe");
            }
        }

        private float _UntererRand;
        public float UntererRand
        {
            get { return _UntererRand; }
            set
            {
                SetProperty(ref _UntererRand, value, "UntererRand");
            }
        }

        private string _meldungtextcolor;
        public string MeldungTextColor
        {
            get { return _meldungtextcolor; }
            set
            {
                SetProperty(ref _meldungtextcolor, value, "MeldungTextColor");
            }
        }
        private string _Artikelnummer;
        public string ArtikelValue
        {
            get { return _Artikelnummer; }
            set
            {
                SetProperty(ref _Artikelnummer, value);
            }
        }

        private string _Lieferscheinnummer;
        public string LieferscheinValue
        {
            get { return _Lieferscheinnummer; }
            set
            {
                SetProperty(ref _Lieferscheinnummer, value);
            }
        }

        private string _strMenge;
        public string StueckValue
        {
            get { return _strMenge; }
            set
            {
                SetProperty(ref _strMenge, value);
            }
        }

        private string _Lagerplatz;
        public string LidValue
        {
            get { return _Lagerplatz; }
            set
            {
                SetProperty(ref _Lagerplatz, value);
            }
        }

        private string _PickerSelItem;
        public string PickerSelItem
        {
            get { return _PickerSelItem; }
            set
            {
                SetProperty(ref _PickerSelItem, value);
            }
        }

        private string _LieferscheinPlaceholder;
        public string LieferscheinPlaceholder
        {
            get { return _LieferscheinPlaceholder; }
            set
            {
                SetProperty(ref _LieferscheinPlaceholder, value, "LieferscheinPlaceholder");
            }
        }

        private string _DemoValue;
        public string DemoValue
        {
            get { return _DemoValue; }
            set
            {
                SetProperty(ref _DemoValue, value, "DemoValue");
            }
        }

        private string _TitleUntermenue;
        public string TitleUntermenue
        {
            get { return _TitleUntermenue; }
            set
            {
                SetProperty(ref _TitleUntermenue, value, "TitleUntermenue");
            }
        }

        private string _ArtikelPlaceholder;
        public string ArtikelPlaceholder
        {
            get { return _ArtikelPlaceholder; }
            set
            {
                SetProperty(ref _ArtikelPlaceholder, value, "ArtikelPlaceholder");
            }
        }

        private string _StueckPlaceholder;
        public string StueckPlaceholder
        {
            get { return _StueckPlaceholder; }
            set
            {
                SetProperty(ref _StueckPlaceholder, value, "StueckPlaceholder");
            }
        }

        private string _LidPlaceholder;
        public string LidPlaceholder
        {
            get { return _LidPlaceholder; }
            set
            {
                SetProperty(ref _LidPlaceholder, value, "LidPlaceholder");
            }
        }

        private bool _LieferscheinVisibility;
        public bool LieferscheinVisibility
        {
            get { return _LieferscheinVisibility; }
            set
            {
                SetProperty(ref _LieferscheinVisibility, value, "LieferscheinVisibility");
            }
        }

        private bool _LieferscheinBarcodeVisibility;
        public bool LieferscheinBarcodeVisibility
        {
            get { return _LieferscheinBarcodeVisibility; }
            set
            {
                SetProperty(ref _LieferscheinBarcodeVisibility, value, "LieferscheinBarcodeVisibility");
            }
        }

        private bool _ArtikelVisibility;
        public bool ArtikelVisibility
        {
            get { return _ArtikelVisibility; }
            set
            {
                SetProperty(ref _ArtikelVisibility, value, "ArtikelVisibility");
            }
        }

        private bool _ArtikelBarcodeVisibility;
        public bool ArtikelBarcodeVisibility
        {
            get { return _ArtikelBarcodeVisibility; }
            set
            {
                SetProperty(ref _ArtikelBarcodeVisibility, value, "ArtikelBarcodeVisibility");
            }
        }

        private bool _StueckVisibility;
        public bool StueckVisibility
        {
            get { return _StueckVisibility; }
            set
            {
                SetProperty(ref _StueckVisibility, value, "StueckVisibility");
            }
        }

        private bool _StueckBarcodeVisibility;
        public bool StueckBarcodeVisibility
        {
            get { return _StueckBarcodeVisibility; }
            set
            {
                SetProperty(ref _StueckBarcodeVisibility, value, "StueckBarcodeVisibility");
            }
        }

        private bool _LidVisibility;
        public bool LidVisibility
        {
            get { return _LidVisibility; }
            set
            {
                SetProperty(ref _LidVisibility, value, "LidVisibility");
            }
        }

        private bool _LidBarcodeVisibility;
        public bool LidBarcodeVisibility
        {
            get { return _LidBarcodeVisibility; }
            set
            {
                SetProperty(ref _LidBarcodeVisibility, value, "LidBarcodeVisibility");
            }
        }

        private Wareneingang valSelectedWE;

        public Wareneingang SelectedWE
        {
            get
            {
                return valSelectedWE;
            }
            set
            {
                if (value != valSelectedWE)
                {
                    valSelectedWE = value;

                    if (value != null)
                    {
                        LieferscheinValue = value.Lieferscheinnummer;
                        ArtikelValue = value.Artikelnummer;
                        StueckValue = value.Menge.ToString();
                        LidValue = value.Lagerplatz;
                    }
                    else
                    {
                        LieferscheinValue = string.Empty;
                        ArtikelValue = string.Empty;
                        StueckValue = string.Empty;
                        LidValue = string.Empty;
                    }
                }
            }
        }

        private void Save()
        {
            try
            {
                Meldung = string.Empty;

                int anz = _dbService.QueryAllWareneingang().Count;

                if (anz >= 5)
                {
                    Meldung = "Demo-Version: Anzahl erreicht.";
                    return;
                }

                Wareneingang row = new Wareneingang();

                if (LieferscheinVisibility && (string.IsNullOrEmpty(_Lieferscheinnummer)))
                {
                    MeldungTextColor = "Red";
                    Meldung = LieferscheinPlaceholder + " eingeben";
                    return;
                }
                if (ArtikelVisibility && (string.IsNullOrEmpty(_Artikelnummer)))
                {
                    MeldungTextColor = "Red";
                    Meldung = ArtikelPlaceholder + " eingeben";
                    return;
                }
                if (StueckVisibility && (string.IsNullOrEmpty(_strMenge)))
                {
                    MeldungTextColor = "Red";
                    Meldung = StueckPlaceholder + " eingeben";
                    return;
                }
                else
                {
                    if (!(IsNumeric(_strMenge)))
                    {
                        MeldungTextColor = "Red";
                        Meldung = StueckPlaceholder + " als Zahl eingeben";
                        return;
                    }
                }
                if (LidVisibility && (string.IsNullOrEmpty(_Lagerplatz)))
                {
                    MeldungTextColor = "Red";
                    Meldung = LidPlaceholder + " eingeben";
                    return;
                }

                row.Benutzer = GlobalData.Global.Benutzer;
                row.Artikelnummer = _Artikelnummer;
                row.Lieferscheinnummer = _Lieferscheinnummer;
                row.Lager = GlobalData.Global.Lager;
                row.Bereich = GlobalData.Global.Bereich;
                row.Datum = DateTime.Now.ToString();

                if (IsNumeric(_strMenge))
                {
                    row.Menge = Convert.ToDouble(_strMenge);
                }
                else
                {
                    row.Menge = 0;
                }

                row.Lagerplatz = _Lagerplatz;

                if (SelectedWE == null)
                {
                    _dbService.InsertRow(row);
                }
                else
                {
                    row.Positionsnummer = SelectedWE.Positionsnummer;
                    _dbService.UpdateRow(row);
                }

                ArtikelValue = string.Empty;
                LieferscheinValue = string.Empty;
                StueckValue = string.Empty;
                LidValue = string.Empty;
                MeldungTextColor = "#089e70";
                Meldung = "Speichern erfolgreich";

                FillListWareneingaenge();
                SelectedWE = null;
            }
            catch (Exception ex)
            {
                MeldungTextColor = "Red";
                Meldung = ex.Message;
            }
        }

        private Command _LieferscheinEntryCommand;
        public Command LieferscheinEntryCommand
        {
            get
            {
                this._LieferscheinEntryCommand = this._LieferscheinEntryCommand ?? new Command<EventArgs>((e) =>
                {
                });
                return this._LieferscheinEntryCommand;
            }
        }
        private Command _ArtikelEntryCommand;
        public Command ArtikelEntryCommand
        {
            get
            {
                this._ArtikelEntryCommand = this._ArtikelEntryCommand ?? new Command<EventArgs>((e) =>
                {
                });
                return this._ArtikelEntryCommand;
            }
        }
        private Command _StueckEntryCommand;
        public Command StueckEntryCommand
        {
            get
            {
                this._StueckEntryCommand = this._StueckEntryCommand ?? new Command<EventArgs>((e) =>
                {
                    Save();
                });
                return this._StueckEntryCommand;
            }
        }
        private Command _LidEntryCommand;
        public Command LidEntryCommand
        {
            get
            {
                this._LidEntryCommand = this._LidEntryCommand ?? new Command<EventArgs>((e) =>
                {
                });
                return this._LidEntryCommand;
            }
        }

        private Command _appearingCommand;
        public Command AppearingCommand
        {
            get
            {
                this._appearingCommand = this._appearingCommand ?? new Command(() =>
                {
                    if (_dbService.GetDemoVersion())
                    {
                        DemoValue = "Logimat 2020";
                    }
                    else
                    {
                        DemoValue = string.Empty;
                    }
                    InitDisplayFelder();
                    FillListEntryProperty();
                    FillListWareneingaenge();
                });
                return this._appearingCommand;
            }
        }
    }
}
