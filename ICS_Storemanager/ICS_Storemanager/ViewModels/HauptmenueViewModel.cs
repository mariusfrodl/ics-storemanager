﻿using abmsoft.mobile.core.Interfaces;
using abmsoft.Mobile.Core.Interfaces;
using abmsoft.Mobile.Core.Models.Database;
using abmsoft.Mobile.Core.Services;
using abmsoft.Mobile.Core.ViewModels;
using ICS_Storemanager.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using XLabs.Forms.Mvvm;

namespace ICS_Storemanager.ViewModels
{

    [ViewType(typeof(Hauptmenue))]
    class HauptmenueViewModel : ViewModelBase
    {
        INavigator _navigationService;
        private IDialogProvider _dialogService;

        public ICommand InventurCommand { get; set; }
        public ICommand BestellerfassungCommand { get; set; }
        public ICommand WareneingangCommand { get; set; }
        public ICommand WarenausgangCommand { get; set; }
        public ICommand UebertragungCommand { get; set; }
        public ICommand EinstellungCommand { get; set; }
        public ICommand ZurueckCommand { get; set; }
        public ObservableCollection<Prozesse> ListProzesse { get; set; }

        DataAccessService _dbService;

        Func<string, HauptmenueViewModel> _MainViewModelFactory;

        public HauptmenueViewModel(INavigator navigationService, Func<string, HauptmenueViewModel> MainViewModelFactory, IDialogProvider dialogService)
        {
            try
            {
                _navigationService = navigationService;
                _MainViewModelFactory = MainViewModelFactory;
                _dialogService = dialogService;
                _dbService = new DataAccessService();
                ListProzesse = new ObservableCollection<Prozesse>();

                TitleHauptmenue = "Hauptmenü";

                this.InventurCommand = new Command<HauptmenueViewModel>(p =>
                {
                    _navigationService.PushAsync<InventurViewModel>();
                });
                this.BestellerfassungCommand = new Command<HauptmenueViewModel>(p =>
                {
                    _navigationService.PushAsync<BestellerfassungViewModel>();
                });
                this.WareneingangCommand = new Command<HauptmenueViewModel>(p =>
                {
                    _navigationService.PushAsync<WareneingangViewModel>();
                });
                this.WarenausgangCommand = new Command<HauptmenueViewModel>(p =>
                {
                    _navigationService.PushAsync<WarenausgangViewModel>();
                });
                this.UebertragungCommand = new Command<HauptmenueViewModel>(async p =>
                {
                    try
                    {
                        var answer = await _dialogService.DisplayAlert("Datenübertragung", "Wollen Sie die erzeugten Daten als CSV - Datei im Verzeichnis Download speichern?", "Ja", "Nein");
                        if (answer)
                        {
                            await _navigationService.PushAsync<DatenuebertragungViewModel>();
                        }
                    }
                    catch (Exception ex)
                    {
                        string Meldung = ex.Message;
                    }
                });
                this.EinstellungCommand = new Command<HauptmenueViewModel>(p =>
                {
                    _navigationService.PushAsync<EinstellungViewModel>();
                });
                ZurueckCommand = new Command(async () =>
                {
                    try
                    {
                        await _navigationService.PopAsync();
                    }
                    catch (Exception ex)
                    {
                       // Meldung = ex.Message;
                    }
             });
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
        }

        private void FillListProzesse()
        {
            SQLiteConnection _conn = Xamarin.Forms.DependencyService.Get<IDatabaseConnection>().DbConnection();

            List<Prozesse> items = _conn.Query<Prozesse>("select * from prozesse");

            ListProzesse.Clear();

            foreach (Prozesse item in items)
            {
                ListProzesse.Add(item);
            }

            if (ListProzesse.Count > 0)
            {
                InitDisplay();
            }
        }
        private void InitDisplayFelder()
        {
            int height = App.screenHeight;
            int width = App.screenWidth;

            ObererRand = (float)(height * 0.057);
            Leerzeile = (float)(height * 0.049);
            Wareneingangzeile = (float)(height * 0.097);
            Warenausgangzeile = (float)(height * 0.097);
            Inventurzeile = (float)(height * 0.097);
            Bestellungzeile = (float)(height * 0.097);
            Meldungzeile = (float)(height * 0.049);

            if (height >= 1200)
            {
                Schrift = 40;
                UntererRand = (float)(height * 0.057);
            }
            else if (height >= 500)
            {
                Schrift = 16;
                UntererRand = (float)(height * 0.097);
            }
            else
            {
                Schrift = 14;
                Wareneingangzeile = (float)(height * 0.1);
                Warenausgangzeile = (float)(height * 0.1);
                Inventurzeile = (float)(height * 0.1);
                Bestellungzeile = (float)(height * 0.1);
                UntererRand = (float)(height * 0.13);
            }

        }

        private void InitDisplay()
        {
            foreach (Prozesse item in ListProzesse)
            {
                switch (item.ProzessID)
                {
                    case "1":
                        WareneingangVisibility = item.Visibility;
                        WareneingangText = item.Bezeichnung;
                        break;
                    case "2":
                        WarenausgangVisibility = item.Visibility;
                        WarenausgangText = item.Bezeichnung;
                        break;
                    case "3":
                        InventurVisibility = item.Visibility;
                        InventurText = item.Bezeichnung;
                        break;
                    case "5":
                        UebertragungVisibility = item.Visibility;
                        DatenübertragungText = item.Bezeichnung;
                        break;
                    case "6":
                        BestellerfassungVisibility = item.Visibility;
                        BestellerfassungText = item.Bezeichnung;
                        break;
                    default:
                        break;
                }
            }
        }

        private string _TitleHauptmenue;
        public string TitleHauptmenue
        {
            get { return _TitleHauptmenue; }
            set
            {
                SetProperty(ref _TitleHauptmenue, value, "TitleHauptmenue");
            }
        }

        private double _Schrift;
        public double Schrift
        {
            get { return _Schrift; }
            set
            {
                SetProperty(ref _Schrift, value, "Schrift");
            }
        }

        private float _ObererRand;
        public float ObererRand
        {
            get { return _ObererRand; }
            set
            {
                SetProperty(ref _ObererRand, value, "ObererRand");
            }
        }

        private float _Leerzeile;
        public float Leerzeile
        {
            get { return _Leerzeile; }
            set
            {
                SetProperty(ref _Leerzeile, value, "Leerzeile");
            }
        }
        private float _Wareneingangzeile;
        public float Wareneingangzeile
        {
            get { return _Wareneingangzeile; }
            set
            {
                SetProperty(ref _Wareneingangzeile, value, "Wareneingangzeile");
            }
        }

        private float _Warenausgangzeile;
        public float Warenausgangzeile
        {
            get { return _Warenausgangzeile; }
            set
            {
                SetProperty(ref _Warenausgangzeile, value, "Warenausgangzeile");
            }
        }

        private float _Inventurzeile;
        public float Inventurzeile
        {
            get { return _Inventurzeile; }
            set
            {
                SetProperty(ref _Inventurzeile, value, "Inventurzeile");
            }
        }

        private float _Bestellungzeile;
        public float Bestellungzeile
        {
            get { return _Bestellungzeile; }
            set
            {
                SetProperty(ref _Bestellungzeile, value, "Bestellungzeile");
            }
        }

        private float _Meldungzeile;
        public float Meldungzeile
        {
            get { return _Meldungzeile; }
            set
            {
                SetProperty(ref _Meldungzeile, value, "Meldungzeile");
            }
        }

        private float _UntererRand;
        public float UntererRand
        {
            get { return _UntererRand; }
            set
            {
                SetProperty(ref _UntererRand, value, "UntererRand");
            }
        }

        private bool _WareneingangVisibility;
        public bool WareneingangVisibility
        {
            get { return _WareneingangVisibility; }
            set
            {
                SetProperty(ref _WareneingangVisibility, value, "WareneingangVisibility");
            }
        }

        private bool _WarenausgangVisibility;
        public bool WarenausgangVisibility
        {
            get { return _WarenausgangVisibility; }
            set
            {
                SetProperty(ref _WarenausgangVisibility, value, "WarenausgangVisibility");
            }
        }

        private bool _InventurVisibility;
        public bool InventurVisibility
        {
            get { return _InventurVisibility; }
            set
            {
                SetProperty(ref _InventurVisibility, value, "InventurVisibility");
            }
        }
        private bool _UebertragungVisibility;
        public bool UebertragungVisibility
        {
            get { return _UebertragungVisibility; }
            set
            {
                SetProperty(ref _UebertragungVisibility, value, "UebertragungVisibility");
            }
        }
        private bool _BestellerfassungVisibility;
        public bool BestellerfassungVisibility
        {
            get { return _BestellerfassungVisibility; }
            set
            {
                SetProperty(ref _BestellerfassungVisibility, value, "BestellerfassungVisibility");
            }
        }

        private string _WareneingangText;
        public string WareneingangText
        {
            get { return _WareneingangText; }
            set
            {
                SetProperty(ref _WareneingangText, value, "WareneingangText");
            }
        }
        private string _WarenausgangText;
        public string WarenausgangText
        {
            get { return _WarenausgangText; }
            set
            {
                SetProperty(ref _WarenausgangText, value, "WarenausgangText");
            }
        }
        private string _InventurText;
        public string InventurText
        {
            get { return _InventurText; }
            set
            {
                SetProperty(ref _InventurText, value, "InventurText");
            }
        }
        private string _DatenübertragungText;
        public string DatenübertragungText
        {
            get { return _DatenübertragungText; }
            set
            {
                SetProperty(ref _DatenübertragungText, value, "DatenübertragungText");
            }
        }
        private string _BestellerfassungText;
        public string BestellerfassungText
        {
            get { return _BestellerfassungText; }
            set
            {
                SetProperty(ref _BestellerfassungText, value, "BestellerfassungText");
            }
        }
        private string _DemoValue;
        public string DemoValue
        {
            get { return _DemoValue; }
            set
            {
                SetProperty(ref _DemoValue, value, "DemoValue");
            }
        }
        private Command _appearingCommand;
        public Command AppearingCommand
        {
            get
            {
                this._appearingCommand = this._appearingCommand ?? new Command(() =>
                {
                    FillListProzesse();

                    if (_dbService.GetDemoVersion())
                    {
                        DemoValue = "Logimat 2020";
                    }
                    else
                    {
                        DemoValue = string.Empty;
                    }
                    InitDisplayFelder();
                });
                return this._appearingCommand;
            }
        }
    }

}
