﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ICS_Storemanager
{
    public partial class App : Application
    {
        public static int screenHeight, screenWidth;

        public App()
        {
            InitializeComponent();
        }

        protected override void OnStart()
        {
            //MessagingCenter.Send<Application>(this,constants)
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
