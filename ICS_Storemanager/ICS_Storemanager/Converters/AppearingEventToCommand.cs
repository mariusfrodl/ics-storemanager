﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Windows.Input;
using Xamarin.Forms;
using System.Reactive.Linq;

namespace ICS_Storemanager.Converters
{
    public class AppearingEventToCommand
    {
        public static readonly BindableProperty ToCommandProperty =
            BindableProperty.CreateAttached<AppearingEventToCommand, ICommand>(
                bindable => AppearingEventToCommand.GetToCommand(bindable),
                null,
                BindingMode.OneWay,
                null,
                (bindable, oldValue, newValue) => AppearingEventToCommand.OnCommandChanged(bindable, oldValue, newValue),
                null,
                null);

        public static readonly BindableProperty FromEventProperty =
            BindableProperty.CreateAttached<AppearingEventToCommand, string>(
                bindable => AppearingEventToCommand.GetFromEvent(bindable),
                null,
                BindingMode.OneWay);

        public static ICommand GetToCommand(BindableObject obj)
        {
            return (ICommand)obj.GetValue(AppearingEventToCommand.ToCommandProperty);
        }

        public static void SetToCommand(BindableObject obj, ICommand value)
        {
            obj.SetValue(AppearingEventToCommand.ToCommandProperty, value);
        }

        public static string GetFromEvent(BindableObject obj)
        {
            return (string)obj.GetValue(AppearingEventToCommand.FromEventProperty);
        }

        public static void SetFromEvent(BindableObject obj, string value)
        {
            obj.SetValue(AppearingEventToCommand.FromEventProperty, value);
        }

        private static void OnCommandChanged(BindableObject obj, ICommand oldValue, ICommand newValue)
        {
            var eventName = GetFromEvent(obj);

            if (string.IsNullOrEmpty(eventName))
            {
                throw new InvalidOperationException("FromEvent property is null or empty");
            }

            Observable.FromEventPattern(obj, eventName).Subscribe(p => {
                var command = GetToCommand(obj);

                if (command != null &&
                    command.CanExecute(p.EventArgs))
                {
                    command.Execute(p.EventArgs);
                }
            });
        }
    }    

}
