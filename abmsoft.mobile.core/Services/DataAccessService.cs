﻿using abmsoft.mobile.core.Interfaces;
using abmsoft.Mobile.Core.Models.Database;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace abmsoft.Mobile.Core.Services
{
    public class DataAccessService
    {
        private SQLiteConnection _conn { get; set; }
        public DataAccessService()
        {
            if (!(Xamarin.Forms.DependencyService.Get<IDatabaseConnection>().ExistsDatabase()))
            {
                _conn = Xamarin.Forms.DependencyService.Get<IDatabaseConnection>().DbConnection();
                CreateDatabase();
            }
            else
            {
                _conn = Xamarin.Forms.DependencyService.Get<IDatabaseConnection>().DbConnection();
            }
        }

        public void CreateDatabase()
        {
            try
            {
                _conn.CreateTable<Wareneingang>();
                _conn.CreateTable<Warenausgang>();
                _conn.CreateTable<Inventur>();
                _conn.CreateTable<Bestellerfassung>();
                _conn.CreateTable<Prozesse>();
                _conn.CreateTable<EntryProperty>();
                _conn.CreateTable<InvSystem>();
                _conn.CreateTable<Config>();

                Prozesse anmeldung = new Prozesse
                {
                    ProzessID = "0",
                    Bezeichnung = "Anmeldung",
                    Backgroundcolor = "Silver",
                    Visibility = true
                };
                _conn.Insert(anmeldung);

                Prozesse wareneingang = new Prozesse
                {
                    ProzessID = "1",
                    Bezeichnung = "Wareneingang",
                    Backgroundcolor = "Silver",
                    Visibility = true
                };
                _conn.Insert(wareneingang);

                Prozesse warenausgang = new Prozesse
                {
                    ProzessID = "2",
                    Bezeichnung = "Warenausgang",
                    Backgroundcolor = "Silver",
                    Visibility = true
                };
                _conn.Insert(warenausgang);

                Prozesse inventur = new Prozesse
                {
                    ProzessID = "3",
                    Bezeichnung = "Inventur",
                    Backgroundcolor = "Silver",
                    Visibility = true
                };
                _conn.Insert(inventur);

                Prozesse bestellerfassung = new Prozesse
                {
                    ProzessID = "6",
                    Bezeichnung = "Bestellerfassung",
                    Backgroundcolor = "Silver",
                    Visibility = true
                };
                _conn.Insert(bestellerfassung);

                Prozesse daten = new Prozesse
                {
                    ProzessID = "5",
                    Bezeichnung = "Datenübertragung",
                    Backgroundcolor = "Silver",
                    Visibility = true

                };
                _conn.Insert(daten);

                EntryProperty benutzer = new EntryProperty
                {
                    FeldID = "0",
                    ProzessID = "0",
                    Bezeichnung = "Benutzer",
                    Backgroundcolor = "Transparent",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(benutzer);

                EntryProperty lager = new EntryProperty
                {
                    FeldID = "1",
                    ProzessID = "0",
                    Bezeichnung = "Lager",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(lager);

                EntryProperty bereich = new EntryProperty
                {
                    FeldID = "2",
                    ProzessID = "0",
                    Bezeichnung = "Bereich",
                    Backgroundcolor = "Yellow",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(bereich);

                EntryProperty welfnr = new EntryProperty
                {
                    FeldID = "0",
                    ProzessID = "1",
                    Bezeichnung = "Lieferscheinnummer",
                    Backgroundcolor = "Transparent",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(welfnr);

                EntryProperty weartnr = new EntryProperty
                {
                    FeldID = "1",
                    ProzessID = "1",
                    Bezeichnung = "Artikelnummer",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(weartnr);

                EntryProperty westk = new EntryProperty
                {
                    FeldID = "2",
                    ProzessID = "1",
                    Bezeichnung = "Stückzahl",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(westk);

                EntryProperty weplatz = new EntryProperty
                {
                    FeldID = "3",
                    ProzessID = "1",
                    Bezeichnung = "Lagerplatz",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(weplatz);

                EntryProperty waartnr = new EntryProperty
                {
                    FeldID = "0",
                    ProzessID = "2",
                    Bezeichnung = "Artikelnummer",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(waartnr);

                EntryProperty wastk = new EntryProperty
                {
                    FeldID = "1",
                    ProzessID = "2",
                    Bezeichnung = "Stückzahl",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(wastk);

                EntryProperty waplatz = new EntryProperty
                {
                    FeldID = "2",
                    ProzessID = "2",
                    Bezeichnung = "Lagerplatz",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(waplatz);

                EntryProperty invartnr = new EntryProperty
                {
                    FeldID = "0",
                    ProzessID = "3",
                    Bezeichnung = "Artikelnummer",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(invartnr);

                EntryProperty invstk = new EntryProperty
                {
                    FeldID = "1",
                    ProzessID = "3",
                    Bezeichnung = "Stückzahl",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(invstk);

                EntryProperty invplatz = new EntryProperty
                {
                    FeldID = "2",
                    ProzessID = "3",
                    Bezeichnung = "Lagerplatz",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(invplatz);

                EntryProperty umplatzvon = new EntryProperty
                {
                    FeldID = "0",
                    ProzessID = "4",
                    Bezeichnung = "Lagerplatz von",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(umplatzvon);

                EntryProperty umartnr = new EntryProperty
                {
                    FeldID = "1",
                    ProzessID = "4",
                    Bezeichnung = "Artikelnummer",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(umartnr);

                EntryProperty umstk = new EntryProperty
                {
                    FeldID = "2",
                    ProzessID = "4",
                    Bezeichnung = "Stückzahl",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(umstk);

                EntryProperty umplatzzu = new EntryProperty
                {
                    FeldID = "3",
                    ProzessID = "4",
                    Bezeichnung = "Lagerplatz zu",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(umplatzzu);

                EntryProperty beartnr = new EntryProperty
                {
                    FeldID = "0",
                    ProzessID = "6",
                    Bezeichnung = "Artikelnummer",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(beartnr);

                EntryProperty bestueck = new EntryProperty
                {
                    FeldID = "1",
                    ProzessID = "6",
                    Bezeichnung = "Stück",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(bestueck);

                EntryProperty begroesse = new EntryProperty
                {
                    FeldID = "2",
                    ProzessID = "6",
                    Bezeichnung = "Grösse",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(begroesse);

                EntryProperty befarbe = new EntryProperty
                {
                    FeldID = "3",
                    ProzessID = "6",
                    Bezeichnung = "Farbe",
                    Backgroundcolor = "White",
                    Placeholdercolor = "#2bbae7",
                    Visibility = true
                };
                _conn.Insert(befarbe);

                InvSystem geraetnr = new InvSystem
                {
                    SystemID = "0",
                    Bezeichnung = "Gerätenummer",
                    Value = "01"
                };
                _conn.Insert(geraetnr);

                InvSystem pwein = new InvSystem
                {
                    SystemID = "1",
                    Bezeichnung = "PW Einstellungen",
                    Value = "0815"
                };
                _conn.Insert(pwein);

                InvSystem pwaus = new InvSystem
                {
                    SystemID = "2",
                    Bezeichnung = "PW Beenden",
                    Value = "4711"
                };
                _conn.Insert(pwaus);

                InvSystem camera_on = new InvSystem
                {
                    SystemID = "3",
                    Bezeichnung = "Kamera an/aus",
                    Value = "1"
                };
                _conn.Insert(camera_on);

                InvSystem deviceID = new InvSystem
                {
                    SystemID = "4",
                    Bezeichnung = "Seriennummer",
                    Value = Xamarin.Forms.DependencyService.Get<IDatabaseConnection>().GetDeviceID()
                };
                _conn.Insert(deviceID);

                InvSystem Lizenz = new InvSystem
                {
                    SystemID = "5",
                    Bezeichnung = "Lizenz",
                    Value = "Demo"
                };
                _conn.Insert(Lizenz);

                InvSystem Demo = new InvSystem
                {
                    SystemID = "6",
                    Bezeichnung = "Demo",
                    Value = "1"
                };
                _conn.Insert(Demo);

                Config DemoAnzWE = new Config
                {
                    ConfigID = "1",
                    Bezeichnung = "DemoAnzWareneingang",
                    Value = "5"
                };
                _conn.Insert(DemoAnzWE);

                Config DemoAnzWA = new Config
                {
                    ConfigID = "2",
                    Bezeichnung = "DemoAnzWarenausgang",
                    Value = "5"
                };
                _conn.Insert(DemoAnzWA);

                Config DemoAnzIN = new Config
                {
                    ConfigID = "3",
                    Bezeichnung = "DemoAnzInventur",
                    Value = "5"
                };
                _conn.Insert(DemoAnzIN);

                Config DemoAnzBE = new Config
                {
                    ConfigID = "5",
                    Bezeichnung = "DemoAnzBestellerfassung",
                    Value = "5"
                };
                _conn.Insert(DemoAnzBE);

            }
            catch (SQLiteException sqlex)
            {
                string message = sqlex.Message;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
        }

        public void UpdateRow(object row)
        {
            _conn.Update(row);

        }

        public void InsertRow(object row)
        {
            _conn.Insert(row);
        }

        public void DeleteRow(object row)
        {
            _conn.Delete(row);
        }

        public string GetGeraetename()
        {
            string ret = string.Empty;

            var result = _conn.Table<InvSystem>().Where(k => k.SystemID == "0").FirstOrDefault();

            if (result != null)
            {
                ret = result.Value;
            }

            return ret;
        }
        public string GetEinstellungPasswort()
        {
            string ret = string.Empty;

            var result = _conn.Table<InvSystem>().Where(k => k.SystemID == "1").FirstOrDefault();

            if (result != null)
            {
                ret = result.Value;
            }

            return ret;
        }
        public string GetLizenzKey()
        {
            string ret = string.Empty;

            var result = _conn.Table<InvSystem>().Where(k => k.SystemID == "5").FirstOrDefault();

            if (result != null)
            {
                ret = result.Value;
            }

            return ret;
        }
        public Boolean GetEinstellungKamera()
        {
            Boolean ret = true;

            var result = _conn.Table<InvSystem>().Where(k => k.SystemID == "3").FirstOrDefault();

            if (result != null)
            {
                if (result.Value == "1")
                {
                    ret = true;
                }
                else
                {
                    ret = false;
                }
            }

            return ret;
        }
        public Boolean GetDemoVersion()
        {
            Boolean ret = true;

            var result = _conn.Table<InvSystem>().Where(k => k.SystemID == "6").FirstOrDefault();

            if (result != null)
            {
                if (result.Value == "1")
                {
                    ret = true;
                }
                else
                {
                    ret = false;
                }
            }

            return ret;
        }
        public Int32 GetDemoAnzahl(string configID)
        {
            Int32 ret = 5;

            var result = _conn.Table<Config>().Where(k => k.ConfigID == configID).FirstOrDefault();

            if (result != null)
            {
                ret = Int32.Parse(result.Value);
            }

            return ret;
        }

        public List<Prozesse> QueryAllProzesse()
        {
            return (from s in _conn.Table<Prozesse>() orderby s.ProzessID select s).ToList();
        }
        public List<EntryProperty> QueryAllEntryProperty()
        {
            return (from s in _conn.Table<EntryProperty>() orderby s.ProzessID, s.FeldID select s).ToList();
        }
        public List<InvSystem> QueryAllInvSystem()
        {
            return (from s in _conn.Table<InvSystem>() orderby s.SystemID select s).ToList();
        }
        public List<EntryProperty> QueryAllEntryProperty(string value)
        {
            return (from s in _conn.Table<EntryProperty>() where s.ProzessID == value orderby s.ProzessID, s.FeldID select s).ToList();
        }
        public List<Wareneingang> QueryAllWareneingang()
        {
            return (from s in _conn.Table<Wareneingang>() orderby s.Positionsnummer select s).ToList();
        }
        public List<Warenausgang> QueryAllWarenausgang()
        {
            return (from s in _conn.Table<Warenausgang>() orderby s.Positionsnummer select s).ToList();
        }
        public List<Inventur> QueryAllInventur()
        {
            return (from s in _conn.Table<Inventur>() orderby s.Positionsnummer select s).ToList();
        }
        public List<Bestellerfassung> QueryAllBestellerfassung()
        {
            return (from s in _conn.Table<Bestellerfassung>() orderby s.Positionsnummer select s).ToList();
        }


        public bool CheckLicense()
        {
            try
            {
                string DeviceID = Xamarin.Forms.DependencyService.Get<IDatabaseConnection>().GetDeviceID();
                string LicenseKey = GetLizenzKey();

                string berkey = UtilMD5.GetHashString("ICS" + DeviceID + "GROUP");

                if (berkey.Length != LicenseKey.Length)
                {
                    return false;
                }

                for (int i = 0; i <= berkey.Length - 1; i++)
                {
                    string o = LicenseKey.Substring(i, 1);
                    string k = berkey.Substring(i, 1);

                    if (k != o)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {

                return false;
            }

        }

        //public string MD5StringHash(string strString)
        //{
        //    MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
        //    byte[] Data = null;
        //    byte[] Result = null;

        //    Data = Encoding.UTF8.GetBytes(strString);

        //    Result = MD5.ComputeHash(Data);

        //    char[] c = new char[Result.Length * 2];
        //    byte b;

        //    for (int bx = 0, cx = 0; bx < Result.Length; ++bx, ++cx)
        //    {
        //        b = ((byte)(Result[bx] >> 4));
        //        c[cx] = (char)(b > 9 ? b - 10 + 'A' : b + '0');

        //        b = ((byte)(Result[bx] & 0x0F));
        //        c[++cx] = (char)(b > 9 ? b - 10 + 'A' : b + '0');
        //    }

        //    return new string(c);
        //}


    }
}

