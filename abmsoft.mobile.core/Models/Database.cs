using SQLite;
using System;
using System.Collections.Generic;

namespace abmsoft.Mobile.Core.Models.Database
{
    public partial class Wareneingang
    {
         [AutoIncrement, PrimaryKey]
        public int Positionsnummer { get; set; }

        [MaxLength(40)]
        public String Lieferscheinnummer { get; set; }

        [MaxLength(50)]
        public String Artikelnummer { get; set; }

        //[MaxLength(10)]
        public Double Menge { get; set; }

        [MaxLength(40)]
        public String Benutzer { get; set; }

        [MaxLength(40)]
        public String Lager { get; set; }

        [MaxLength(50)]
        public String Bereich { get; set; }

        [MaxLength(50)]
        public String Lagerplatz { get; set; }

        [MaxLength(50)]
        public String Datum { get; set; }

        [MaxLength(50)]
        public String Uhrzeit { get; set; }

        public double Schriftgroesse { get; set; }

        //public override string ToString()
        //{
        //    return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", WAID, Name, Strasse, PLZ, Ort, Land, Zusatz, Email, Webseite);
        //}

    }
    public partial class Warenausgang 
    {
        [AutoIncrement, PrimaryKey]
        public int Positionsnummer { get; set; }

        [MaxLength(50)]
        public String Artikelnummer { get; set; }

        //[MaxLength(10)]
        public Double Menge { get; set; }

        [MaxLength(40)]
        public String Benutzer { get; set; }

        [MaxLength(40)]
        public String Lager { get; set; }

        [MaxLength(50)]
        public String Bereich { get; set; }

        [MaxLength(50)]
        public String Lagerplatz { get; set; }

        [MaxLength(50)]
        public String Datum { get; set; }

        [MaxLength(50)]
        public String Uhrzeit { get; set; }
        public double Schriftgroesse { get; set; }

        //public override string ToString()
        //{
        //    return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", WAID, Name, Strasse, PLZ, Ort, Land, Zusatz, Email, Webseite);
        //}

    }
    public partial class Inventur
    {
        //[PrimaryKey, AutoIncrement]
        //public int IVID { get; set; }

        [AutoIncrement, PrimaryKey]
        public int Positionsnummer { get; set; }

        [MaxLength(50)]
        public String Artikelnummer { get; set; }

        //[MaxLength(10)]
        public Double Menge { get; set; }

        [MaxLength(40)]
        public String Benutzer { get; set; }

        [MaxLength(40)]
        public String Lager { get; set; }

        [MaxLength(50)]
        public String Bereich { get; set; }

        [MaxLength(50)]
        public String Lagerplatz { get; set; }

        [MaxLength(50)]
        public String Datum { get; set; }

        [MaxLength(50)]
        public String Uhrzeit { get; set; }
        public double Schriftgroesse { get; set; }

        //public override string ToString()
        //{
        //    return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", WAID, Name, Strasse, PLZ, Ort, Land, Zusatz, Email, Webseite);
        //}

    }

    public partial class Bestellerfassung
    {
        [AutoIncrement, PrimaryKey]
        public int Positionsnummer { get; set; }

        [MaxLength(50)]
        public String Artikelnummer { get; set; }

        //[MaxLength(10)]
        public Double Menge { get; set; }

        [MaxLength(40)]
        public String Benutzer { get; set; }

        [MaxLength(40)]
        public String Lager { get; set; }

        [MaxLength(50)]
        public String Bereich { get; set; }

        [MaxLength(50)]
        public String Farbe { get; set; }

        [MaxLength(50)]
        public String Groesse { get; set; }

        [MaxLength(50)]
        public String Datum { get; set; }

        [MaxLength(50)]
        public String Uhrzeit { get; set; }

        public double Schriftgroesse { get; set; }
    }

    public partial class InvSystem
    {
        [PrimaryKey, MaxLength(40)]
        public String SystemID { get; set; }

        [MaxLength(50)]
        public String Bezeichnung { get; set; }

        [MaxLength(50)]
        public String Value { get; set; }
    }

    public partial class Prozesse
    {
        [PrimaryKey, MaxLength(40)]
        public String ProzessID { get; set; }

        [MaxLength(50)]
        public String Bezeichnung { get; set; }

        //[MaxLength(10)]
        public bool Visibility { get; set; }

        [MaxLength(40)]
        public String Backgroundcolor { get; set; }
    }

    public partial class EntryProperty
    {
        [AutoIncrement, PrimaryKey]
        public int ID { get; set; }

        [MaxLength(40)]
        public String FeldID { get; set; }

        [MaxLength(40)]
        public String ProzessID { get; set; }

        [MaxLength(50)]
        public String Bezeichnung { get; set; }

        //[MaxLength(10)]
        public bool Visibility { get; set; }

        [MaxLength(40)]
        public String Backgroundcolor { get; set; }

        [MaxLength(40)]
        public String Placeholdercolor { get; set; }
    }

    public partial class Config
    {
        [PrimaryKey, MaxLength(40)]
        public String ConfigID { get; set; }

        [MaxLength(50)]
        public String Bezeichnung { get; set; }

        [MaxLength(50)]
        public String Value { get; set; }
    }
}